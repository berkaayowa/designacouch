-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for disigncouch
DROP DATABASE IF EXISTS `disigncouch`;
CREATE DATABASE IF NOT EXISTS `disigncouch` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `disigncouch`;

-- Dumping structure for table disigncouch.about
DROP TABLE IF EXISTS `about`;
CREATE TABLE IF NOT EXISTS `about` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `content` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.about: ~0 rows (approximately)
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` (`id`, `title`, `content`) VALUES
	(2, 'About Us', NULL);
/*!40000 ALTER TABLE `about` ENABLE KEYS */;

-- Dumping structure for table disigncouch.aboutus
DROP TABLE IF EXISTS `aboutus`;
CREATE TABLE IF NOT EXISTS `aboutus` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(10000) DEFAULT NULL,
  `content` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.aboutus: ~0 rows (approximately)
/*!40000 ALTER TABLE `aboutus` DISABLE KEYS */;
INSERT INTO `aboutus` (`id`, `title`, `content`) VALUES
	(1, 'About Us', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc\r\n\r\n<div><br></div><div>\r\n\r\n<b>There are many variations of passages </b>of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc\r\n\r\n<br></div><div><br></div><div>\r\n\r\nThere are many variations of passages o<u><b>f Lorem Ipsum available, but the majorit</b></u>y have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc</div><div><br></div><div>\r\n\r\n<b>slightly believable. If you are going to&nbsp;</b></div><div><br></div><div>use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic wo\r\n\r\n<br></div><div>\r\n\r\nslightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic wo\r\n\r\n&nbsp;\r\n\r\n<br></div>');
/*!40000 ALTER TABLE `aboutus` ENABLE KEYS */;

-- Dumping structure for table disigncouch.category
DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.category: ~6 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `code`, `name`, `isDeleted`) VALUES
	(1, 'SH', 'Second Hand', 0),
	(2, 'NM', 'New Material', 0),
	(3, NULL, 'Test', 1),
	(4, NULL, 'ddd', 1),
	(5, NULL, 'bbfbf', 1),
	(6, NULL, 'bbfbf', 1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table disigncouch.color
DROP TABLE IF EXISTS `color`;
CREATE TABLE IF NOT EXISTS `color` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `IsDeleted` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.color: ~4 rows (approximately)
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` (`id`, `code`, `name`, `description`, `IsDeleted`) VALUES
	(1, 'RD', 'Red', NULL, 0),
	(2, 'BK', 'Black', NULL, 0),
	(3, 'WT', 'White', NULL, 0),
	(4, 'PK', 'Pink', NULL, 0);
/*!40000 ALTER TABLE `color` ENABLE KEYS */;

-- Dumping structure for table disigncouch.contact
DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mobileNumber` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `physicalAddress` varchar(300) DEFAULT NULL,
  `facebook` varchar(300) DEFAULT NULL,
  `twitter` varchar(300) DEFAULT NULL,
  `whatsapp` varchar(100) DEFAULT NULL,
  `imo` varchar(100) DEFAULT NULL,
  `feedback` varchar(500) DEFAULT NULL,
  `map` varchar(1000) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.contact: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` (`id`, `mobileNumber`, `email`, `physicalAddress`, `facebook`, `twitter`, `whatsapp`, `imo`, `feedback`, `map`, `isDeleted`) VALUES
	(1, '712531147', 'ayowaberka@gmail.com', '54 Cape Suites\r\nCorner De Villiers and Constitution streetseddd', NULL, NULL, NULL, NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- Dumping structure for table disigncouch.currency
DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.currency: ~0 rows (approximately)
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;

-- Dumping structure for table disigncouch.home
DROP TABLE IF EXISTS `home`;
CREATE TABLE IF NOT EXISTS `home` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) DEFAULT NULL,
  `subTitle` varchar(1000) DEFAULT NULL,
  `imageUrl` varchar(1000) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.home: ~0 rows (approximately)
/*!40000 ALTER TABLE `home` DISABLE KEYS */;
INSERT INTO `home` (`id`, `title`, `subTitle`, `imageUrl`, `isDeleted`) VALUES
	(1, 'Need A Couch Made To Fit In Your Space?', 'Give Us a call today Tel: 021 948 9931 Cell: 076 412 8391', 'images/45ef4a19bb7aca08022e8bf0800c66e5.png', 0);
/*!40000 ALTER TABLE `home` ENABLE KEYS */;

-- Dumping structure for table disigncouch.image
DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `refProductId` int(10) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `isDefault` tinyint(1) DEFAULT 0,
  `isDeleted` tinyint(1) DEFAULT 0,
  `createdDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.image: ~23 rows (approximately)
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` (`id`, `refProductId`, `url`, `isDefault`, `isDeleted`, `createdDate`) VALUES
	(1, 736, 'images/cf9123a8fc7460653408492896701c3d.png', 0, 1, NULL),
	(2, 736, 'images/838586d476b70ebcc7a6716ea6d64fac.png', 0, 1, NULL),
	(3, 736, 'images/0e2d018103d92eed923cfd22bd5edd3c.png', 1, 1, NULL),
	(4, 737, 'images/09b68db2d02a3dec69c31e940e663de5.png', 1, 1, NULL),
	(5, 737, 'images/d2592315c2aa4ddf77e7a457adacc22c.png', 0, 1, NULL),
	(6, 737, 'images/644f098d810109cfc4289e2517ba0435.png', 0, 1, NULL),
	(7, 737, 'images/a16e1085275a169362b9d1663460c0c2.png', 0, 1, NULL),
	(8, 737, 'images/59e007ad6f0dfa73cd533638e1eafae6.png', 0, 1, NULL),
	(9, 737, 'images/5c9bef039f12cc5a8d948355ae224d09.png', 0, 1, NULL),
	(10, 737, 'images/8a8f41833877b5ab47ce17087e088f50.png', 0, 1, NULL),
	(11, 738, 'images/9caa40ef301e822c4431a8dab5b021da.png', 1, 1, NULL),
	(12, 739, 'images/961129488d7c5771b04d5086cef5d946.png', 1, 1, NULL),
	(13, 738, 'images/73bc8e4850c280e7330921e195a379e2.png', 1, 1, NULL),
	(14, 736, 'images/71af04b0a47d264135957b4d1579e641.png', 0, 1, NULL),
	(15, 737, 'images/f110121212b8a68d8910e4014d4f619e.png', 1, 1, NULL),
	(16, 736, 'images/c4b008ca7a358cebcdeded76dac2223d.png', 0, 1, NULL),
	(17, 737, 'images/10be23aff060e80cde683d5af74c854b.png', 1, 0, NULL),
	(18, 739, 'images/ec73a78780fe33c0204e0e21ec215a0a.png', 1, 0, NULL),
	(19, 738, 'images/a0ec065b081dc2d9dacc76431a1f3407.png', 1, 0, NULL),
	(20, 736, 'images/98cf92c258b717903aa5e519fa2b8dce.png', 1, 0, NULL),
	(21, 740, 'images/219c43c7477be24a05b1399e6aa84f69.png', 1, 1, NULL),
	(22, 740, 'images/8a4cfa9864f8e940d2cfb10761e31dc2.png', 0, 0, NULL),
	(23, 740, 'images/bd660acd1f6088877f4d92d8bdca2d9d.png', 1, 0, NULL),
	(24, 737, 'images/4f66e99bb7d5d15a9377448c4863b168.png', 0, 0, NULL),
	(25, 736, 'images/711b4e32d73a7e0ef74684e1df30f626.png', 0, 0, NULL),
	(26, 741, 'images/c9a75c76836f7f749fcd525b9a240fcd.png', 1, 0, NULL),
	(27, 742, 'images/b35f6c5332de0e18adee0aaa0897c4e3.png', 1, 0, NULL),
	(28, 743, 'images/9fa7dfe486493b2b4998e8507edaa189.png', 1, 0, NULL);
/*!40000 ALTER TABLE `image` ENABLE KEYS */;

-- Dumping structure for table disigncouch.linkedtag
DROP TABLE IF EXISTS `linkedtag`;
CREATE TABLE IF NOT EXISTS `linkedtag` (
  `LinkID` int(10) NOT NULL AUTO_INCREMENT,
  `RefTagID` int(10) DEFAULT NULL,
  `RefSubtagID` int(10) DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`LinkID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.linkedtag: ~0 rows (approximately)
/*!40000 ALTER TABLE `linkedtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `linkedtag` ENABLE KEYS */;

-- Dumping structure for table disigncouch.message
DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `MessageID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Message` varchar(100) DEFAULT NULL,
  `IsRead` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`MessageID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.message: ~0 rows (approximately)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;

-- Dumping structure for table disigncouch.page
DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `PageID` int(10) NOT NULL AUTO_INCREMENT,
  `Title` varchar(250) DEFAULT NULL,
  `Content` varchar(50000) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT current_timestamp(),
  `LastModified` datetime DEFAULT current_timestamp(),
  `IsVisible` tinyint(1) DEFAULT 1,
  `Link` varchar(280) DEFAULT NULL,
  `Banner` longblob DEFAULT NULL,
  `IsPost` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`PageID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.page: ~0 rows (approximately)
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
/*!40000 ALTER TABLE `page` ENABLE KEYS */;

-- Dumping structure for table disigncouch.product
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `refCategoryId` int(10) DEFAULT NULL,
  `refStatusId` int(10) DEFAULT NULL,
  `seats` int(10) DEFAULT NULL,
  `name` varchar(290) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `summary` varchar(1000) DEFAULT NULL,
  `price` varchar(50) DEFAULT '0',
  `latestPrice` varchar(20) NOT NULL DEFAULT '0',
  `isActive` tinyint(1) DEFAULT 1,
  `createdDate` datetime DEFAULT current_timestamp(),
  `expireDate` datetime DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `imageUrl` varchar(500) DEFAULT NULL,
  `imageName` varchar(100) DEFAULT 'no_image.jpg',
  `tags` varchar(100) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT 0,
  `refColorId` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=745 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.product: ~5 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `refCategoryId`, `refStatusId`, `seats`, `name`, `description`, `summary`, `price`, `latestPrice`, `isActive`, `createdDate`, `expireDate`, `link`, `imageUrl`, `imageName`, `tags`, `isDeleted`, `refColorId`) VALUES
	(736, 1, 1, 2, '6-Piece lounge sutie', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula ', '5000', '0', 1, '2019-05-26 08:12:56', '2019-05-26 08:12:56', NULL, '', 'no_image.jpg', NULL, 0, 1),
	(737, 1, 1, 2, '2 Division couch that can be converted ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula ', '8000', '0', 1, '2019-05-26 08:16:16', NULL, NULL, '', 'no_image.jpg', NULL, 0, 1),
	(738, 1, 1, 0, 'Crawford 3 seater couch', '<p></p>\r\n\r\nThis massive 3-division couch offers sophisticated living at its best. Elegantly designed with deep buttoned seat and backrest for comfort and style. Made in a luxurious fabric and features a double studded arm.\r\n\r\n<p></p><p><br></p>', 'This massive 3-division couch offers sophisticated living at its best. Elegantly designed with deep buttoned seat and backrest for comfort and style. Made in a luxurious fabric and features a double studded arm.', '0', '0', 1, '2019-05-26 08:20:00', NULL, NULL, '', 'no_image.jpg', NULL, 0, 4),
	(739, 1, 1, 2, '2-Piece corner lounge suite', '<p>&nbsp;</p>', 'Classy fabric corner suite with wooden legs', '0', '0', 1, '2019-06-16 08:16:52', NULL, NULL, '', 'no_image.jpg', NULL, 0, 2),
	(740, 1, 1, 3, 'Krama 2 seater couch 2', '<p>\r\n\r\n2-Division couch that offers sophisticated living at its best. Elegantly designed with deep buttoned seat and backrest for comfort and style. Made in a luxurious fabric and features a double studded arm.\r\n\r\n<br></p>', '2-Division couch that offers sophisticated living at its best. Elegantly designed with deep buttoned seat and backrest for comfort and style. Made in a luxurious fabric and features a double studded arm.', '150', '0', 1, '2019-06-16 14:12:56', NULL, NULL, '', 'no_image.jpg', NULL, 0, 2),
	(741, 2, 1, 3, 'Avianto lounge suite 2', '<p>\r\n\r\nUpholstered in luxurious Turkey fabric and uniqueness it will bring a style of sophistication into you home.\r\n\r\n<br></p>', 'Upholstered in luxurious Turkey fabric and uniqueness it will bring a style of sophistication into you home.', '2000', '0', 1, '2019-06-16 15:28:14', NULL, NULL, '', 'no_image.jpg', NULL, 0, 3),
	(742, 1, 1, 3, 'kitty 3 seater couch', '<p>\r\n\r\nThis massive 3-division couch offers sophisticated living at its best. Elegantly designed with deep buttoned seat and backrest for comfort and style. Made in a luxurious fabric and features a double studded arm.\r\n\r\n<br></p>', 'This massive 3-division couch offers sophisticated living at its best. Elegantly designed with deep buttoned seat and backrest for comfort and style. Made in a luxurious fabric and features a double studded arm.', '4000', '0', 1, '2019-06-16 15:35:05', NULL, NULL, '', 'no_image.jpg', NULL, 0, 1),
	(743, 1, 1, 3, 'Chateau fabric couch 2 seater', '<p>\r\n\r\nThis couch offers sophisticated living at its best. Elegantly designed for comfort and style with luxurious fabric and cushions.\r\n\r\n<br></p>', 'This couch offers sophisticated living at its best. Elegantly designed for comfort and style with luxurious fabric and cushions.', '1500', '0', 1, '2019-06-16 15:38:13', NULL, NULL, '', 'no_image.jpg', NULL, 0, 1),
	(744, 1, 1, 0, 'aaa', '<p>aaa</p>', 'aaa', '0', '0', 1, '2019-06-16 16:25:29', NULL, NULL, '', 'no_image.jpg', NULL, 1, 1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table disigncouch.product_seat
DROP TABLE IF EXISTS `product_seat`;
CREATE TABLE IF NOT EXISTS `product_seat` (
  `SeatID` int(10) NOT NULL AUTO_INCREMENT,
  `Code` varchar(100) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Value` int(11) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `IsDeleted` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`SeatID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.product_seat: ~4 rows (approximately)
/*!40000 ALTER TABLE `product_seat` DISABLE KEYS */;
INSERT INTO `product_seat` (`SeatID`, `Code`, `Name`, `Value`, `Description`, `IsDeleted`) VALUES
	(1, 'ONE', '1 seat', 1, 'seat', 0),
	(2, 'TWO', '2 seats', 2, NULL, 0),
	(3, 'FOR', '4 seats', 4, NULL, 0),
	(4, 'TWR', '3 seats', 3, NULL, 0);
/*!40000 ALTER TABLE `product_seat` ENABLE KEYS */;

-- Dumping structure for table disigncouch.product_type
DROP TABLE IF EXISTS `product_type`;
CREATE TABLE IF NOT EXISTS `product_type` (
  `TypeID` int(10) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`TypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.product_type: ~3 rows (approximately)
/*!40000 ALTER TABLE `product_type` DISABLE KEYS */;
INSERT INTO `product_type` (`TypeID`, `Code`, `Name`, `Description`, `IsDeleted`) VALUES
	(1, 'LC', '100% Leather Couches ', NULL, 0),
	(2, 'FUC', 'Fully Upholstered Couches', NULL, 0),
	(3, 'SC', 'Slipcover Couches ', NULL, 0);
/*!40000 ALTER TABLE `product_type` ENABLE KEYS */;

-- Dumping structure for table disigncouch.status
DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.status: ~2 rows (approximately)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` (`id`, `code`, `name`, `isDeleted`) VALUES
	(1, 'ACT', 'Active', 0),
	(2, 'INACT', 'Inactive', 0);
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

-- Dumping structure for table disigncouch.subtag
DROP TABLE IF EXISTS `subtag`;
CREATE TABLE IF NOT EXISTS `subtag` (
  `SubTagID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(500) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`SubTagID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.subtag: ~0 rows (approximately)
/*!40000 ALTER TABLE `subtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `subtag` ENABLE KEYS */;

-- Dumping structure for table disigncouch.tag
DROP TABLE IF EXISTS `tag`;
CREATE TABLE IF NOT EXISTS `tag` (
  `TagID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`TagID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.tag: ~0 rows (approximately)
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;

-- Dumping structure for table disigncouch.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(200) DEFAULT '',
  `LastName` varchar(200) DEFAULT '',
  `refRoleID` int(10) DEFAULT 3,
  `phone` varchar(50) DEFAULT NULL,
  `emailAddress` varchar(500) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `resetPasswordLink` varchar(1000) DEFAULT NULL,
  `verificationCode` varchar(1000) DEFAULT NULL,
  `isVerified` tinyint(1) DEFAULT 0,
  `requirePasswordChange` tinyint(1) DEFAULT 0,
  `canLogIn` tinyint(1) DEFAULT 0,
  `isDeleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_users_user_roles` (`refRoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `firstName`, `LastName`, `refRoleID`, `phone`, `emailAddress`, `password`, `lastUpdated`, `resetPasswordLink`, `verificationCode`, `isVerified`, `requirePasswordChange`, `canLogIn`, `isDeleted`) VALUES
	(32, 'Berka', 'Ayowa', 3, NULL, 'ayowaberka@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, NULL, 1, 0, 0, 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table disigncouch.user_role
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role` (
  `RoleID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table disigncouch.user_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
