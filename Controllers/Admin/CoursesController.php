<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\DateTime;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;
    use Util\Helper;


    class CoursesController extends BerkaPhpController
	{

        private $resources ;

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index($option = null) {

            $tag = '';

            $courses = T::Find('course')
                ->Join('status', 'status.id = course.refStatusId')
                ->Join(['user'=>'tutor'], 'tutor.id = course.refUserId')
                ->Join('category', 'category.id = course.refCategoryId')
                ->Where('course.isDeleted' ,'=', Check::$False);

            if(sizeof($option['args']['query']) > 0 && array_key_exists('tag', $option['args']['query'])) {
                $tag = $option['args']['query']['tag'];
                $courses->Where('course.name' ,' like ', '%'.$tag.'%');
            }

            $courses = $courses->FetchList();

            $tutors = T::Find('user')
                ->Join('role', 'role.Id = user.refRoleId')
                ->Join('status', 'status.Id = user.refStatusId')
                ->Join(['user_type'=>'type'], 'type.id = user.refUserTypeId')
                ->Where('user.isDeleted', '=', Check::$False)
                ->Where('type.code', '=', TUTOR_CODE)
                ->FetchList(['assocArray'=>true]);

            $status = T::Find('status')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $category = T::Find('category')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            array_unshift($category, ['id'=>"", 'name'=>DROP_DOWN]);
            array_unshift($status, ['id'=>"", 'name'=>DROP_DOWN, 'code'=>'...']);
            array_unshift($tutors, ['id'=>"", 'name'=>DROP_DOWN, 'surname'=>'']);

            $this->view->set('category', $category);
            $this->view->set('status', $status);
            $this->view->set('tutors', $tutors);
            $this->view->set('breadcrumb', 'Courses');
            $this->view->set('courses', $courses);
            $this->view->set('tag', $tag);
			$this->view->render();

		}

        function add($option = null) {

            $categories = T::Find('category')
                ->Where('isDeleted' ,'=', Check::$False)
                ->FetchList();


            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $course = T::Create('course');
                $course->SetProperties($data);

                $status = T::Find('status')
                ->Where('code', '=','PND')
                ->Where('isDeleted', '=', 0)
                ->FetchFirstOrDefault();

                if(!$status->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'not  status' ,'success'=>false]);

                if(\BerkaPhp\Helper\Auth::GetActiveUser()->role->code != "ADM") {
                    $course->refStatusId = $status->id;

                    if(\BerkaPhp\Helper\Auth::GetActiveUser()->role->code == TUTOR_CODE)
                        $course->refUserId = \BerkaPhp\Helper\Auth::GetActiveUser()->id;
                }

                $course->createdBy = \BerkaPhp\Helper\Auth::GetActiveUser()->id;
                $course->createdDate = DateTime::toDate(NOW, DATE_FORMAT);

                if ($course->Save()) {

                    return $this->jsonFormat(['error'=>false, 'message'=>"le cours a ete ajoute avec succes en attente d'approbation", 'success'=>true, 'link'=>'/admin/courses']);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>"Une erreur survenue n'a pas pu sauver le cours" ,'success'=>false]);
                }

            }

            $status = T::Find('status')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $category = T::Find('category')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $tutors = T::Find('user')
                ->Join('role', 'role.Id = user.refRoleId')
                ->Join('status', 'status.Id = user.refStatusId')
                ->Join(['user_type'=>'type'], 'type.id = user.refUserTypeId')
                ->Where('user.isDeleted', '=', Check::$False)
                ->Where('type.code', '=', TUTOR_CODE)
                ->FetchList(['assocArray'=>true]);

            $this->view->set('status', $status);
            $this->view->set('tutors', $tutors);
            $this->view->set('categories', $category);
            $this->view->set('breadcrumb', 'Cours / Nouveau');
            $this->view->render();

        }

        function edit($option = null) {

            $course = T::Find('course')
                ->Join('status', 'status.Id = course.refStatusId')
                ->Where('course.id','=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                if(!$course->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this status' ,'success'=>false]);

                $course->SetProperties($data);

                if(\BerkaPhp\Helper\Auth::GetActiveUser()->role->code != "ADM") {

                    if($course->status->code == S_ACTIVE) {
                        return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Vous ne pouvez pas mettre � jour le cours, il est d�j� en direct") ,'success'=>false]);
                    }
                }

                $course->modifiedBy = \BerkaPhp\Helper\Auth::GetActiveUser()->id;
                $course->modifiedDate = DateTime::toDate(NOW, DATE_FORMAT);

                if ($course->Save()) {

                    return $this->jsonFormat(['error'=>false, 'message'=>"le cours a ete modifie' avec succes en attente d'approbation", 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>"Une erreur survenue n'a pas pu modifier le cours" ,'success'=>false]);
                }

            }

            $status = T::Find('status')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $category = T::Find('category')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $tutors = T::Find('user')
                ->Join('role', 'role.Id = user.refRoleId')
                ->Join('status', 'status.Id = user.refStatusId')
                ->Join(['user_type'=>'type'], 'type.id = user.refUserTypeId')
                ->Where('user.isDeleted', '=', Check::$False)
                ->Where('type.code', '=', TUTOR_CODE)
                ->FetchList(['assocArray'=>true]);


            $this->view->set('course', $course);
            $this->view->set('status', $status);
            $this->view->set('tutors', $tutors);
            $this->view->set('categories', $category);
            $this->view->set('breadcrumb', 'Cours / Nouveau');
            $this->view->render();

        }

        function delete($option) {

            $course = T::Find('course')
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if($course != null ) {

                if(!$course->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this currency' ,'success'=>false]);

                $course->isDeleted = Check::$True;

                if ($course->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("Le donne'e a �t� supprimer avec succ�s"), 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de supprimer ce donne'e") ,'success'=>false]);
                }

            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'No data provided' ,'success'=>false]);
            }

        }

        function price($option) {

            $courseId = $option['args']['params'][0];

            $modules = T::Find('module')
                ->Join('status', 'status.id = module.refStatusId')
                ->Join('course', 'course.id = module.refCourseId')
                ->Where('module.isDeleted' ,'=', Check::$False)
                ->Where('module.refCourseId' ,'=', $courseId)
                ->Where('status.code' ,'=', S_ACTIVE)
                ->OrderBy('module.morder', 'ASC')
                ->FetchList();

            $price = array_reduce($modules, function($i, $module)
            {
                return $i + $module->price;
            });

//            if($price)

            return $this->jsonFormat(['error'=>false,'message'=>false, 'success'=>true, 'data'=>['price'=>'$'.$price]]);
        }



    }

?>