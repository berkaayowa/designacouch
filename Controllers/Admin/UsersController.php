<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\DateTime;
    use BerkaPhp\Helper\Debug;
    use BerkaPhp\Helper\Rand;
    use BrkORM\T;
    use Helper\Check;
    use Util\Helper;

    class UsersController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);

		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index($option = null) {

            $users = T::Find('user')
                ->Join('role', 'role.Id = user.refRoleId')
                ->Join('status', 'status.Id = user.refStatusId')
                ->Where('user.isDeleted', '=', 0)
                ->FetchList();

            if(isset($option['args']['query']['min'])) {
                $this->view->set('min', (double) $option['args']['query']['min']);
            }

            if(isset($option['args']['query']['max'])) {
                $this->view->set('max', (double) $option['args']['query']['max']);
            }

			$this->view->set('users', $users);
			$this->view->render();

		}

        function add() {

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $user = T::Create('user');
                $user->SetProperties($data);
                $user->username = $user->mobileNumber;
                $user->password = md5($user->password);
                $user->accountNumber = Helper::generateUniqueAccountNumber();
                $user->confirmationCode = substr(Rand::uniqueDigit(1000, 10000), 0, 4);

                if ($user->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'User as been added successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not added user' ,'success'=>false]);
                }

            }

            $roles = T::Find('role')->FetchList(['assocArray'=>true]);
            $status = T::Find('status')->FetchList(['assocArray'=>true]);

            $this->view->set('roles', $roles);
            $this->view->set('status', $status);

            $this->view->render();
        }

        function edit($option) {

            $data = $this->getPost();

            $user = T::Find('user')
                ->Join('role', 'role.Id = user.refRoleId')
                ->Join('status', 'status.Id = user.refStatusId')
                ->Where('user.id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if(sizeof($data) > 0) {

                if(!$user->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this user' ,'success'=>false]);

                $user->SetProperties($data);

                if ($user->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'User has been updated successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not updated user' ,'success'=>false]);
                }

            }

            $roles = T::Find('role')->FetchList(['assocArray'=>true]);
            $status = T::Find('status')->FetchList(['assocArray'=>true]);

            $this->view->set('user', $user);

            $this->view->set('roles', $roles);
            $this->view->set('status', $status);

            $this->view->render();
        }

        function delete($option) {

            $user = T::Find('user')
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if($user != null ) {

                if(!$user->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this user' ,'success'=>false]);

                $user->isDeleted = Check::$True;

                if ($user->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'User has been deleted successfully', 'success'=>true, 'redirect'=>'/admin/users']);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not delete user' ,'success'=>false]);
                }

            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'No data provided' ,'success'=>false]);
            }

        }

        function balance($option = null) {
            $data =  Helper::CalculateBalance(['id'=>$option['args']['params'][0]]);
            return $this->jsonFormat(['error'=>false, 'message'=>'' ,'success'=>true, 'data' => $data]);
        }

        function statment($option = null) {

            $dates = explode('-', $option['args']['query']['date']);
            $transactionRecords = [];

            $createdTransactions = T::Find('transaction')
                ->Join(['transaction_status'=>'status'], 'status.id = transaction.refStatusId')
                ->Join(['transaction_type'=>'type'], 'type.id = transaction.refTypeId')
                ->Join('currency', 'currency.id = transaction.refCurrencyId')
                ->Join(['user'=>'author'], 'author.id = transaction.createdBy')
                ->Join(['user'=>'user'], 'user.id = transaction.refUserId', 'LEFT JOIN')
                ->Where('transaction.createdBy', '=', $option['args']['params'][0])
                ->Where('transaction.isDeleted', '=', Check::$False)

                ->Where('transaction.createdDate', '>=', DateTime::toDate($dates[0], DATE_FORMAT))
                ->Where('transaction.createdDate', '<=', DateTime::toDate($dates[1], DATE_FORMAT))
                ->Where('status.code', '=', TRAN_APPROVED)
                ->OrderBy('transaction.createdDate', 'ASC')
                ->FetchList();

            $transactionsReceived = T::Find('transaction')
                ->Join(['transaction_status'=>'status'], 'status.id = transaction.refStatusId')
                ->Join(['transaction_type'=>'type'], 'type.id = transaction.refTypeId')
                ->Join('currency', 'currency.id = transaction.refCurrencyId')
                ->Join(['user'=>'author'], 'author.id = transaction.createdBy')
                ->Join(['user'=>'user'], 'user.id = transaction.refUserId', 'LEFT JOIN')
                ->Where('transaction.refUserId', '=', $option['args']['params'][0])
                ->Where('transaction.isDeleted', '=', Check::$False)

                ->Where('transaction.createdDate', '>=', DateTime::toDate($dates[0], DATE_FORMAT))
                ->Where('transaction.createdDate', '<=', DateTime::toDate($dates[1], DATE_FORMAT))
                ->Where('status.code', '=', TRAN_APPROVED)
                ->OrderBy('transaction.createdDate', 'ASC')
                ->FetchList();

            $transactions = array_merge($createdTransactions, $transactionsReceived);

            usort($transactions, function($a, $b)
            {
                return $a->id > $b->id;
            });

            $balance = Helper::CalculateBalance(['id'=>$option['args']['params'][0], 'endDate'=>DateTime::toDate($dates[0], DATE_FORMAT)])->balance->approved;
         //   $balance = number_format($balance, 2);
            $transactionRecord['date'] = DateTime::toDate($dates[0], DATE_FORMAT);
            $transactionRecord['id'] = '';
            $transactionRecord['amount'] = number_format($balance, 2);
            $transactionRecord['type'] = 'balance';
            $transactionRecord['description'] = 'balance forwarded';
            $transactionRecord['balance'] = number_format($balance, 2);
            $transactionRecord['credit'] =  number_format(0, 2);
            $transactionRecord['debit'] = number_format(0, 2);

            array_push($transactionRecords, $transactionRecord);

            $ids = [];
            foreach ($transactions as $transaction) {

                if(in_array($transaction->id, $ids))
                    continue;

                $transactionRecord['date'] = $transaction->createdDate;
                $transactionRecord['id'] = $transaction->id;
                $transactionRecord['amount'] = number_format($transaction->amount, 2);

                if($transaction->type->code == 'TPU') {

                    $transactionRecord['credit'] = $transactionRecord['amount'];
                    $transactionRecord['debit'] = number_format(0, 2);
                    $transactionRecord['type'] = 'credit';
                    $transactionRecord['description'] = 'top up';
                    $balance = $balance + $transaction->amount;
                    $transactionRecord['balance'] = number_format($balance, 2);

                } else  if($transaction->type->code == 'TSFM' && $transaction->author->id == $option['args']['params'][0]) {

                    $transactionRecord['debit'] = $transactionRecord['amount'];
                    $transactionRecord['credit'] = number_format(0, 2);

                    $transactionRecord['type'] = 'debit';
                    $transactionRecord['description'] = 'transfer to '.$transaction->user->name .' #'.$transaction->user->accountNumber;
                    $balance = $balance - $transaction->amount;
                    $transactionRecord['balance'] = number_format($balance, 2);;

                } else  if($transaction->type->code == 'TSFM' && $transaction->author->id != $option['args']['params'][0] && $transaction->user->id == $option['args']['params'][0]) {

                    $transactionRecord['credit'] = $transactionRecord['amount'];
                    $transactionRecord['debit'] = number_format(0, 2);

                    $transactionRecord['type'] = 'Credit';
                    $transactionRecord['description'] = 'Transfered from '.$transaction->author->name .' #'.$transaction->author->accountNumber;
                    $balance = $balance + $transaction->amount;
                   // Debug::PrintOut(($balance));
                    $transactionRecord['balance'] = number_format($balance, 2);

                } else if($transaction->type->code == 'SDM' && $transaction->author->id == $option['args']['params'][0]) {

                    $transactionRecord['debit'] = $transactionRecord['amount'];
                    $transactionRecord['credit'] = number_format(0, 2);

                    $transactionRecord['type'] = 'debit';
                    $transactionRecord['description'] = 'sent money to #'.$transaction->user->name;
                    $balance = $balance - $transaction->amount;
                    $transactionRecord['balance'] = number_format($balance, 2);;

                } else if($transaction->type->code == 'SDM' && $transaction->author->id != $option['args']['params'][0] && $transaction->user->id == $option['args']['params'][0]) {

                    $transactionRecord['credit'] = $transactionRecord['amount'];
                    $transactionRecord['debit'] = number_format(0, 2);

                    $transactionRecord['type'] = 'Credit';
                    $transactionRecord['description'] = 'Received from '.$transaction->author->name .' #'.$transaction->author->accountNumber;
                    $balance = $balance + $transaction->amount;
                    $transactionRecord['balance'] = number_format($balance, 2);

                }

                else if($transaction->type->code == 'CWL') {

                    $transactionRecord['debit'] = $transactionRecord['amount'];
                    $transactionRecord['credit'] = number_format(0, 2);

                    $transactionRecord['type'] = 'debit';
                    $transactionRecord['description'] = 'Cash Withdraw';
                    $balance = $balance - $transaction->amount;

                    $transactionRecord['balance'] = number_format($balance, 2);;

                }

                array_push($transactionRecords, $transactionRecord);
                array_push($ids, $transactionRecord['id']);

            }

            $user = T::Find('user')
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            $this->view->set('dateFrom', DateTime::toDate($dates[0], DATE_FORMAT_To_DISPLAY));
            $this->view->set('dateTo', DateTime::toDate($dates[1], DATE_FORMAT_To_DISPLAY));
            $this->view->set('transactionRecords', $transactionRecords);
            $this->view->set('user', $user);
            $this->view->render();
        }



    }

?>