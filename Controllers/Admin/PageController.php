<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;


    class PageController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

			$this->view->render();

		}


        function aboutus($option = null) {

            $aboutus = T::Find('aboutus')
                ->FetchFirstOrDefault();

            $data = $this->getPost();

            if (sizeof($data) > 0) {

                if (!$aboutus->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => 'Opps! could not find terms and conditions', 'success' => false]);

                $aboutus->SetProperties($data);
                $aboutus->content = utf8_decode($data['content']);

                if ($aboutus->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error' => false, 'message' => 'Record has been updated successfully', 'success' => true]);
                } else {
                    return $this->jsonFormat(['error' => true, 'message' => 'Error could not updated', 'success' => false]);
                }

            }else {

                $this->view->set('data', $aboutus);
                $this->view->render();
            }
        }

        function home($option = null) {

            $home = T::Find('home')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            $data = $this->getPost();

            if (sizeof($data) > 0) {

                if (!$home->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => 'Opps! could not find data', 'success' => false]);

                $home->SetProperties($data);

                if ($home->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error' => false, 'message' => 'Record has been updated successfully', 'success' => true]);
                } else {
                    return $this->jsonFormat(['error' => true, 'message' => 'Error could not updated', 'success' => false]);
                }

            }else {

                $this->view->set('data', $home);
                $this->view->render();
            }
        }




	}

?>