<?php
namespace Controller\Admin;
use BerkaPhp\Controller\BerkaPhpController;
use BerkaPhp\Helper\Debug;
use BrkORM\T;

class StatusController extends BerkaPhpController
{

    function __construct() {
        parent::__construct(false);
    }

    /* Display all users from database
    *  Client action in this controller
    *  @author berkaPhp
    */

    function index() {

        $status = T::Find('status')
            ->Where('status.isDeleted', '=', 0)
            ->FetchList();

        $this->view->set('status', $status);

        $this->view->render();

    }

    function edit($option) {

        $data = $this->getPost();

        $status = T::Find('status')
            ->Where('status.id', '=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        if(sizeof($data) > 0) {

            if(!$status->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this status' ,'success'=>false]);

            $status->name = $data['name'];

            if ($status->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>'Status has been updated successfully', 'success'=>true]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'Error could not updated status' ,'success'=>false]);
            }

        }

        $this->view->set('status', $status);

        $this->view->render();
    }


}

?>