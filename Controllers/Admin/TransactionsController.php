<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Auth;
    use BerkaPhp\Helper\DateTime;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;
    use Util\Helper;

    class TransactionsController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $data = T::Find('transaction')
                ->Join(['transaction_status'=>'status'], 'status.id = transaction.refStatusId')
                ->Join(['transaction_type'=>'type'], 'type.id = transaction.refTypeId')
                ->Join(['user'=>'author'], 'author.id = transaction.createdBy')
                ->Join(['user'=>'receiver'], 'receiver.id = transaction.refUserId', 'LEFT JOIN')
                ->Join('currency', 'currency.id = transaction.refCurrencyId', 'LEFT JOIN')
                ->Where('transaction.isDeleted', '=', Check::$False)
                ->OrderBy('transaction.createdDate', 'DESC')
                ->FetchList();

            $this->view->set('transactions', $data);
            $this->view->render();

		}

        function add($option = mull) {


            $data = $this->getPost();
            $code = $option['args']['query']['code'];

            $type = T::Find('transaction_type')
                ->Where('isDeleted', '=', Check::$False)
                ->Where('code', '=', $code)
                ->FetchFirstOrDefault();

            $pendingStatus = T::Find('transaction_status')
                ->Where('isDeleted', '=', Check::$False)
                ->Where('code', '=', TRAN_PENDING)
                ->FetchFirstOrDefault();

            $currency = T::Find('currency')
                ->Where('isDeleted', '=', Check::$False)
                ->Where('isDefault', '=', Check::$True)
                ->Where('code', '=', 'ZAR')
                ->FetchFirstOrDefault();

            if(!$type->IsAny()) {

            }

            if(sizeof($data) > 0) {


                $transaction = T::Create('transaction');
                $transaction->SetProperties($data);
                $transaction->refTypeId = $type->id;
                $transaction->refStatusId = $pendingStatus->id;
                $transaction->createdDate = DateTime::toDate(NOW, DATE_FORMAT);

                $user = T::Find('user')
                    ->Join('status', 'status.Id = user.refStatusId')
                    ->Where('user.id', '=', $transaction->createdBy)
                    ->Where('user.isDeleted', '=', Check::$False)
                    ->FetchFirstOrDefault();

                if(!$user->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'The sending account could not be found' ,'success'=>false, 'code'=>0]);

                if($user->status->code == 'APD')
                    return $this->jsonFormat(['error'=>true, 'message'=>'The sending account account has been suspended, contact support for more' ,'success'=>false, 'code'=>3, 'user'=> $user]);

                if($user->status->code == 'PFC')
                    return $this->jsonFormat(['error'=>true, 'message'=>'The sending account account is not verified yet' ,'success'=>false, 'code'=>2, 'user'=> $user]);

                if($user->status->code == 'RVCR')
                    return $this->jsonFormat(['error'=>true, 'message'=>'The sending account OTP has been expired' ,'success'=>false, 'code'=>6, 'user'=> $user]);

                $defaultCurrency = T::Find('currency')
                    ->Where('isDefault', '=', Check::$True)
                    ->Where('isDeleted', '=', Check::$False)
                    ->FetchFirstOrDefault();

                $rate = new \stdClass();
                $rate->value = 1;

                if (!$defaultCurrency->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => '!Oops the system could not find default currency, contact support', 'success' => false]);


                if($defaultCurrency->id != $data['refCurrencyId']) {
                    $rate = T::Find('rate')
                        ->Join('currency', 'currency.id = rate.refCurrencyId')
                        ->Join(['currency' => 'base'], 'base.id = rate.refBaseCurrencyId')
                        ->Where('rate.refCurrencyId', '=', $data['refCurrencyId'])
                        ->Where('base.isDefault', '=', Check::$True)
                        ->Where('base.isDeleted', '=', Check::$False)
                        ->Where('rate.isDeleted', '=', Check::$False)
                        ->FetchFirstOrDefault();

                    if (!$rate->IsAny())
                        return $this->jsonFormat(['error' => true, 'message' => '!Oops the system could not find default currency to initialize the transaction', 'success' => false]);

                }

                $transaction->refOriginCurrencyId = $transaction->refCurrencyId;
                $transaction->refCurrencyId = $defaultCurrency->id;
                $convertedAmount = ($data['amount'] * $rate->value);
                $transaction->originAmount = $transaction->amount;
                $transaction->rate = $rate->value;
                $transaction->amount = $convertedAmount;

                if($type->code != 'TPU') {


                    $user = T::Find('user')
                        ->Join('status', 'status.Id = user.refStatusId')
                        ->Where('user.id', '=', $transaction->refUserId)
                        ->Where('user.isDeleted', '=', Check::$False)
                        ->FetchFirstOrDefault();

                    if(!$user->IsAny())
                        return $this->jsonFormat(['error'=>true, 'message'=>'The receiver account could not be found' ,'success'=>false, 'code'=>0]);

                    if($user->status->code == 'APD')
                        return $this->jsonFormat(['error'=>true, 'message'=>'The receiver account account has been suspended, contact support for more' ,'success'=>false, 'code'=>3, 'user'=> $user]);

                    if($user->status->code == 'PFC')
                        return $this->jsonFormat(['error'=>true, 'message'=>'The receiver account account is not verified yet' ,'success'=>false, 'code'=>2, 'user'=> $user]);

                    if($user->status->code == 'RVCR')
                        return $this->jsonFormat(['error'=>true, 'message'=>'The receiver OTP has been expired' ,'success'=>false, 'code'=>6, 'user'=> $user]);

                    if($transaction->refUserId == $transaction->createdBy)
                        return $this->jsonFormat(['error'=>true, 'message'=>'You can not perform transaction to the same account' ,'success'=>false, 'code'=>7]);

                    $userBalance = Helper::CalculateBalance(['id' =>   $transaction->createdBy]);
                    $convertedBalance = ($userBalance->balance->approved * $rate->value);

                    if (!($convertedBalance >= $convertedAmount))
                        return $this->jsonFormat(['error' => true, 'message' => '!Oops transaction could not be initialized, you do not have enough balance', 'success' => false]);

                }

                if ($transaction->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'Transaction has been created successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not create transaction' ,'success'=>false]);
                }

            }

            $types = T::Find('transaction_type')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);
            $status = T::Find('transaction_status')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);
            $users = T::Find('user')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $currencies = T::Find('currency')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);




            $this->view->set('currencies', $currencies);
            $this->view->set('types', $types);
            $this->view->set('status', $status);
            $this->view->set('users', $users);

            $this->view->set('type', $type);
            $this->view->set('pendingStatus', $pendingStatus);
            $this->view->set('currency', $currency);

            $this->view->render();
        }

        function edit($option) {

            $data = $this->getPost();

            $transaction = T::Find('transaction')
                ->Where('isDeleted', '=', Check::$False)
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if(sizeof($data) > 0) {

                if(!$transaction->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! no transaction found' ,'success'=>false]);

                $transaction->SetProperties($data);
                $transaction->lastModifiedDate = DateTime::toDate(NOW, DATE_FORMAT);
                $transaction->lastModifiedBy = Auth::GetActiveUser()->id;

                if ($transaction->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'User as been updated successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not updated user' ,'success'=>false]);
                }

            }

            $types = T::Find('transaction_type')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);
            $status = T::Find('transaction_status')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);
            $users = T::Find('user')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $currencies = T::Find('currency')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $this->view->set('currencies', $currencies);
            $this->view->set('types', $types);
            $this->view->set('status', $status);
            $this->view->set('users', $users);
            $this->view->set('transaction', $transaction);


            $this->view->render();
        }

        function delete($option) {

            $transaction = T::Find('transaction')
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if($transaction != null ) {

                if(!$transaction->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! no transaction found' ,'success'=>false]);

                $transaction->isDeleted = Check::$True;

                if ($transaction->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'Transaction has been deleted successfully', 'success'=>true, 'redirect'=>'/admin/transactions']);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not delete this transaction' ,'success'=>false]);
                }

            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'No data provided' ,'success'=>false]);
            }

        }

        function action(){

            $data = $this->getPost();

            if (sizeof($data) > 0) {

                $transaction = T::Find('transaction')
                    ->Join(['transaction_type'=>'type'], 'type.id = transaction.refTypeId')
                    ->Join(['user'=>'author'], 'author.id = transaction.createdBy')
                    ->Join(['user'=>'receiver'], 'receiver.id = transaction.refUserId', 'LEFT JOIN')
                    ->Join('currency', 'currency.id = transaction.refCurrencyId', 'LEFT JOIN')
                    ->Where('transaction.isDeleted', '=', Check::$False)
                    ->Where('transaction.id', '=', $data['id'])
                    ->FetchFirstOrDefault();

                if (!$transaction->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => 'Opps! no transaction found', 'success' => false]);

                $status = T::Find('transaction_status')
                    ->Where('isDeleted', '=', Check::$False);

                if($data['action'] =='approve'){
                    $status->Where('code', '=', 'APV');
                } else if($data['action'] =='cancel') {
                    $status->Where('code', '=', 'CLD');
                }

                $status->FetchFirstOrDefault();

                if (!$status->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => 'Opps! no transaction status  found', 'success' => false]);

                $transaction->comment = $data['comment'];
                $transaction->refStatusId = $status->id;
                $transaction->lastModifiedDate = DateTime::toDate(NOW, DATE_FORMAT);
                $transaction->lastModifiedBy = Auth::GetActiveUser()->id;

                if ($transaction->Save()) {

                    $sms = null;
                    $tel = null;

                    if($transaction->type->code == 'SDM' || $transaction->type->code == 'TSFM') {
                        $tel = $transaction->receiver->mobileNumber;
                        $sms = "ZaharaPay :) \\nDear : " .$transaction->receiver->name . ' ' .$transaction->receiver->surname .' you have been sent money on ZahraPay and transaction is '.$status->name.' ,Your Reference number is #'.$transaction->id.' \\nRegards\\nZaharaPay Team' ;
                    } else if($transaction->type->code == 'TPU') {
                        $tel = $transaction->author->mobileNumber;
                        $sms = "ZaharaPay :) \\nDear : " .$transaction->author->name . ' ' .$transaction->author->surname .' your Top Up request has been '.$status->name.',Your Reference number is #'.$transaction->id.' \\nRegards\\nZaharaPay Team' ;
                    }

//                    try{
//                        if($sms != null && $tel != null)
//                            @Helper::SendSMS(['to'=>$tel, 'message'=>$sms]);
//                    }catch (\Exception $ex) {
//
//                    }

                    sleep(1);
                    return $this->jsonFormat(['error' => false, 'message' => 'Transaction has been '.($data['action'] == 'approve' ? 'approved' : 'cancelled').' successfully', 'success' => true, 'redirect'=>'/admin/transactions']);
                } else {
                    return $this->jsonFormat(['error' => true, 'message' => 'Error could not updated '.($data['action'] == 'approve' ? 'approved' : 'cancelled').' transaction', 'success' => false]);
                }

            } else {
                return $this->jsonFormat(['error' => true, 'message' => 'Error could not update , data data provided', 'success' => false]);
            }
        }


    }

?>