<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;
    use Util\Helper;

    class ProductsController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct();
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index($option) {

            $products = T::Find('product')
                ->Join('category', 'category.id = product.refCategoryId')
                ->Join('status', 'status.id = product.refStatusId')
                ->Where('product.isDeleted', '=', Check::$False);


            if(sizeof($option['args']['query']) > 0 && !empty($option['args']['query']['statusId'])) {
                $products->Where('product.refStatusId', '=', $option['args']['query']['statusId']);
            }

            if(sizeof($option['args']['query']) > 0 && !empty($option['args']['query']['refCategoryId'])) {
                $products->Where('product.refCategoryId', '=', $option['args']['query']['refCategoryId']);
            }

            $products = $products->FetchList();

            $this->view->set('products', $products);
			$this->view->render();

		}

        function add() {

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $category = T::Create('product');

                $category->SetProperties($data);

                if ($category->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("New record has been added successfully"), 'success'=>true, 'link'=>'/admin/products']);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("!Oops could not add this record, try again") ,'success'=>false]);
                }

            }

            $this->view->render();

        }

        function edit($option){

            $data = $this->getPost();

            $product = T::Find('product')
                ->Where('isDeleted', '=', Check::$False)
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if (sizeof($data) > 0) {

                if (!$product->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => 'Opps! could not find a record', 'success' => false]);

                $product->SetProperties($data);

                if ($product->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error' => false, 'message' => 'Record has been updated successfully', 'success' => true]);
                } else {
                    return $this->jsonFormat(['error' => true, 'message' => 'Error a record could not be updated ', 'success' => false]);
                }

            }

            $this->view->set('product', $product);
            $this->view->render();
        }

        function image($option){
           // Debug::PrintOut($_FILES);
           $this->view->render();
        }

        function delete($option) {

            $product = T::Find('product')
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if($product != null ) {

                if(!$product->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find data' ,'success'=>false]);

                $product->isDeleted = Check::$True;

                if ($product->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'Product has been deleted successfully', 'success'=>true, 'link'=>'/admin/products']);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Product could not be deleted, try again' ,'success'=>false]);
                }

            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'No data provided' ,'success'=>false]);
            }

        }


    }

?>