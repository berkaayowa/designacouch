<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;

    class TermsController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

        function index() {

            $data = T::Find('terms_and_conditions')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            $this->view->set('data', $data);
            $this->view->render();

        }

        function edit($option){

            $data = $this->getPost();



            if (sizeof($data) > 0) {

                $termsData = T::Find('terms_and_conditions')
                    ->Where('isDeleted', '=', Check::$False)
                    ->Where('id', '=', $option['args']['params'][0])
                    ->FetchFirstOrDefault();

                if (!$termsData->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => 'Opps! could not find terms and conditions', 'success' => false]);

                $termsData->SetProperties($data);

                if ($termsData->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error' => false, 'message' => 'terms and conditions has been updated successfully', 'success' => true]);
                } else {
                    return $this->jsonFormat(['error' => true, 'message' => 'Error could not updated terms and conditions', 'success' => false]);
                }

            }
        }



    }

?>