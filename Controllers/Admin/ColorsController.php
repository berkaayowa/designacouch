<?php
namespace Controller\Admin;
use BerkaPhp\Controller\BerkaPhpController;
use BerkaPhp\Helper\Debug;
use BrkORM\T;

class ColorsController extends BerkaPhpController
{

    function __construct() {
        parent::__construct();
    }

    /* Display all users from database
    *  Client action in this controller
    *  @author berkaPhp
    */

    function index() {

        $status = T::Find('color')
            ->Where('isDeleted', '=', 0)
            ->FetchList();

        $this->view->set('colors', $status);

        $this->view->render();

    }

    function edit($option) {

        $data = $this->getPost();

        $status = T::Find('color')
            ->Where('id', '=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        if(sizeof($data) > 0) {

            if(!$status->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this record' ,'success'=>false]);

            $status->name = $data['name'];

            if ($status->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>'Record has been updated successfully', 'success'=>true]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'Error could not updated a record' ,'success'=>false]);
            }

        }

        $this->view->set('color', $status);

        $this->view->render();
    }


}

?>