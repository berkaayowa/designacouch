<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;

    class ExchangeController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $data = T::Find('currency')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList();

            $this->view->set('currencies', $data);
            $this->view->render();

		}

        function add() {

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $currency = T::Create('currency');
                $currency->SetProperties($data);

                if ($currency->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'Currency has been added successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Currency could not add currency' ,'success'=>false]);
                }

            }

            $this->view->render();

        }

        function edit($option) {

            $data = $this->getPost();

            $currency = T::Find('currency')
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if(sizeof($data) > 0) {

                if(!$currency->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this currency' ,'success'=>false]);

                $currency->SetProperties($data);

                if ($currency->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'Currency been updated successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Currency could not updated user' ,'success'=>false]);
                }

            }

            $this->view->set('currency', $currency);
            $this->view->render();

        }

        function delete($option) {

            $currency = T::Find('currency')
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if($currency != null ) {

                if(!$currency->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this currency' ,'success'=>false]);

                $currency->isDeleted = Check::$True;

                if ($currency->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'Currency has been deleted successfully', 'success'=>true, 'redirect'=>'/admin/exchange']);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Currency could not updated user' ,'success'=>false]);
                }

            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'No data provided' ,'success'=>false]);
            }

        }

        function currency ($option = null) {

            $base = T::Find('currency')
                ->Where('id', '=', $option['args']['params'][0])
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            $currencies = T::Find('currency')
                ->Where('id', '<>', $base->id)
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList();

            $data = $this->getPost();

            if(sizeof($data) > 0){

                $count = 0;
                foreach($currencies as $currency) {

                    if(array_key_exists($currency->code, $data)) {

                        $rate = T::Find('rate')
                            ->Where('refBaseCurrencyId', '=', $base->id)
                            ->Where('refCurrencyId', '=', $currency->id)
                            ->FetchFirstOrDefault();

                        if($rate->IsAny()) {
                            $rate->value = $data[$currency->code];
                        } else {
                            $rate = T::Create('rate');
                            $rate->refBaseCurrencyId = $base->id;
                            $rate->refCurrencyId = $currency->id;
                            $rate->value = $data[$currency->code];
                        }

                        if ($rate->Save())
                            $count++;



                    }
                }

                if ($count > 0) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'Rate updated successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Rate could not updated user' ,'success'=>false]);
                }

            }

            $this->view->set('currencies', $currencies);
            $this->view->set('base', $base);
            $this->view->render();
        }


    }

?>