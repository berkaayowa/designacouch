<?php
namespace Controller\Admin;
use BerkaPhp\Controller\BerkaPhpController;
use BerkaPhp\Helper\Debug;
use BrkORM\T;
use Helper\Check;
use Util\Helper;
;

class ImagesController extends BerkaPhpController
{

    function __construct() {
        parent::__construct();
    }

    /* Display all users from database
    *  Client action in this controller
    *  @author berkaPhp
    */

    function index($option) {

        $images = T::Find('image')
            ->Where('image.isDeleted', '=', Check::$False)
            ->Where('image.refProductId', '=', $option['args']['params'][0])
            ->FetchList();

        $product = T::Find('product')
            // ->join('image', 'image.refProductId = product.id', 'LEFT JOIN')
            ->Where('isDeleted', '=', Check::$False)
            // ->Where('image.isDefault', '=', Check::$True)
            ->Where('id', '=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        $this->view->set('product', $product);
        $this->view->set('images', $images);
        $this->view->render();

    }

    function setdefault($option) {

        $images = T::Find('image')
            ->Where('image.isDeleted', '=', Check::$False)
            ->Where('image.isDefault', '=', Check::$True)
            ->Where('image.refProductId', '=', $option['args']['params'][1])
            ->FetchList();

        foreach($images as $image) {

            $image = T::Find('image')
                ->Where('isDeleted', '=', Check::$False)
                ->Where('id', '=',$image->id)
                ->FetchFirstOrDefault();

            $image->isDefault = Check::$False;
            $image->Save();
        }

        $image = T::Find('image')
            ->Where('isDeleted', '=', Check::$False)
            ->Where('id', '=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        $image->isDefault = Check::$True;

        if ($image->Save()) {
            sleep(1);
            return $this->jsonFormat(['error' => false, 'message' => 'Image has been set default successfully', 'success' => true, 'link'=>'/admin/images/index/'.$option['args']['params'][1]]);
        } else {
            return $this->jsonFormat(['error' => true, 'message' => 'Error a could not be set default image ', 'success' => false]);
        }

    }

    function maintain($option) {

        $product = T::Find('product')
            ->Where('isDeleted', '=', Check::$False)
            ->Where('id', '=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        if(sizeof($this->getPost()) > 0) {

            $isUploaded = false;

            $fileName = "upload";
            $data = $this->getPost();

            if ($_FILES[$fileName]['error'] !== UPLOAD_ERR_OK) {
                return $this->jsonFormat(['success'=>false, 'message'=>'!oop file error', 'error'=>true]);
            }

            $file = $_FILES[$fileName]['tmp_name'];

            $f_name= $_FILES[$fileName]['name'];
            $parts = explode(".", $f_name);
            $end = end($parts);
            $extension = strtolower($end);

            $mime = $_FILES[$fileName]['type'];

            $identity = str_replace("-", '', \BerkaPhp\Helper\Rand::newGuid());

            $document = T::Create('image');

            $document->refProductId = $option['args']['params'][0];

            $remotePath = "";

            switch ($mime) {
                case 'image/jpeg':
                case 'image/png':
                    $remotePath = "images/";
                    break;
                case 'application/pdf':
                case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                    $remotePath = "documents/";
                    break;
                default:
                    return $this->jsonFormat(['success'=>false, 'message'=>'invalid file type {'.$mime.'}', 'error'=>true]);
            }

            $document->url = $remotePath.$identity.".".$extension;;

            $isUploaded = $this->uploadToFtp($file, $document->url);

            if($isUploaded && $document->Save())
                return $this->jsonFormat(['success'=>true, 'message'=>'Uploaded successfully', 'error'=>false, 'link'=>'/images/index/'.$option['args']['params'][0]]);
            else
                return $this->jsonFormat(['success'=>false, 'message'=>'Error could not upload successfully', 'error'=>true]);

        }

        else
        {
            $this->view->set('product', $product);
            $this->view->render();
        }


    }

    function uploadToFtp($file, $fileName) {

//        $ftp_conn = @ftp_connect(FTP_HOST);
//        $login = @ftp_login($ftp_conn, FTP_USERNAME, FTP_PASSWORD);
//        $upload = @ftp_put($ftp_conn, $fileName, $file, FTP_BINARY);
//        @ftp_close($ftp_conn);
//
//        if ($upload) {
//            return true;
//        }
//        else {
//            return false;
//        }
        $ftp_conn = @ftp_connect(FTP_HOST);
        //tp_pasv($ftp_conn, true);
        $login = @ftp_login($ftp_conn, FTP_USERNAME, FTP_PASSWORD);
        ftp_pasv($ftp_conn, true);
        $upload = @ftp_put($ftp_conn, $fileName, $file,FTP_BINARY);

        @ftp_close($ftp_conn);

        if ($upload || empty($upload)) {
            return true;
        }
        else {
            return false;
        }


    }

    function delete($option) {

        $course = T::Find('image')
            ->Where('id', '=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        if($course != null ) {

            if(!$course->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this currency' ,'success'=>false]);

            $course->isDeleted = Check::$True;

            if ($course->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>'Image has been deleted successfully', 'success'=>true, 'link'=>'/admin/images/index/'.$option['args']['params'][1]]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'Image could bot be deleted, try again' ,'success'=>false]);
            }

        } else {
            return $this->jsonFormat(['error'=>true, 'message'=>'No data provided' ,'success'=>false]);
        }

    }

    function home($option) {

        $home = T::Find('home')
            ->Where('isDeleted', '=', Check::$False)
            ->FetchFirstOrDefault();

        if(sizeof($this->getPost()) > 0) {

            $isUploaded = false;

            $fileName = "upload";
            $data = $this->getPost();

            if ($_FILES[$fileName]['error'] !== UPLOAD_ERR_OK) {
                return $this->jsonFormat(['success'=>false, 'message'=>'!oop file error', 'error'=>true]);
            }

            $file = $_FILES[$fileName]['tmp_name'];

            $f_name= $_FILES[$fileName]['name'];
            $parts = explode(".", $f_name);
            $end = end($parts);
            $extension = strtolower($end);

            $mime = $_FILES[$fileName]['type'];

            $identity = str_replace("-", '', \BerkaPhp\Helper\Rand::newGuid());

            $remotePath = "";

            switch ($mime) {
                case 'image/jpeg':
                case 'image/png':
                    $remotePath = "images/";
                    break;
                case 'application/pdf':
                case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                    $remotePath = "documents/";
                    break;
                default:
                    return $this->jsonFormat(['success'=>false, 'message'=>'invalid file type {'.$mime.'}', 'error'=>true]);
            }

            $home->imageUrl = $remotePath.$identity.".".$extension;;

            $isUploaded = $this->uploadToFtp($file, $home->imageUrl);

            if($isUploaded && $home->Save())
                return $this->jsonFormat(['success'=>true, 'message'=>'Uploaded successfully', 'error'=>false]);
            else
                return $this->jsonFormat(['success'=>false, 'message'=>'Error could not upload successfully', 'error'=>true]);

        }

        else
        {
            $this->view->set('home', $home);
            $this->view->render();
        }


    }


}

?>