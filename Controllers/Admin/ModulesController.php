<?php
namespace Controller\Admin;
use BerkaPhp\Controller\BerkaPhpController;
use BerkaPhp\Helper\Auth;
use BerkaPhp\Helper\DateTime;
use BerkaPhp\Helper\Debug;
use BrkORM\T;
use Helper\Check;
use Util\Helper;

class ModulesController extends BerkaPhpController
{

    function __construct() {
        parent::__construct(false);
    }

    /* Display all users from database
    *  Client action in this controller
    *  @author berkaPhp
    */

    function index($option) {

        $courseId = $option['args']['params'][0];

        $modules = T::Find('module')
            ->Join('status', 'status.id = module.refStatusId')
            ->Join('course', 'course.id = module.refCourseId')
            ->Where('module.isDeleted' ,'=', Check::$False)
            ->Where('module.refCourseId' ,'=', $courseId)
            ->OrderBy('module.morder', 'ASC')
            ->FetchList();

        $course = T::Find('course')
            ->Join('status', 'status.Id = course.refStatusId')
            ->Where('course.id','=', $courseId)
            ->FetchFirstOrDefault();

        $this->view->set('course', $course);
        $this->view->set('modules', $modules);

        $this->view->render();

    }

    function edit($option) {

        $data = $this->getPost();
        $courseId = $option['args']['params'][1];

        $courses = T::Find('course')
            ->Where('isDeleted' ,'=', Check::$False)
            ->FetchList(['assocArray'=>true]);
        $course = T::Find('course')
            ->Where('isDeleted' ,'=', Check::$False)
            ->Where('id' ,'=', $courseId)
            ->FetchFirstOrDefault();

        $module = T::Find('module')
            ->Join('status', 'status.id = module.refStatusId')
            ->Where('module.isDeleted' ,'=', Check::$False)
            ->Where('module.id' ,'=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        if(sizeof($data) > 0) {

            if(!$module->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur , aucun module trouve'") ,'success'=>false]);

            if($module->status->code == S_ACTIVE) {
                if(Auth::GetActiveUser()->role->code != "ADM") {
                    return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de modifier ce donnee, c est deja en ligne") ,'success'=>false]);
                }
            }

            $module->SetProperties($data);

            $module->modifiedBy = \BerkaPhp\Helper\Auth::GetActiveUser()->id;
            $module->modifiedDate = DateTime::toDate(NOW, DATE_FORMAT);

            if ($module->Save()) {
//                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("le donnee a ete modifier avec succes"), 'success'=>true]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de modifier ce donneee") ,'success'=>false]);
            }

        }

        $status = T::Find('status')
            ->Where('isDeleted', '=', Check::$False)
            ->FetchList(['assocArray'=>true]);

        $this->view->set('status', $status);

        $this->view->set('module', $module);
        $this->view->set('courses', $courses);
        $this->view->set('course', $course);
        $this->view->render();
    }

    function add($option) {

        $data = $this->getPost();
        $courseId = $option['args']['params'][0];

        $course = T::Find('course')
            ->Where('isDeleted' ,'=', Check::$False)
            ->Where('id' ,'=', $courseId)
            ->FetchFirstOrDefault();

        if(sizeof($data) > 0) {

            if(!$course->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de ajouter ce donne'ee, pas de cous trouve'") ,'success'=>false]);

            $module = T::Create('module');
            $module->SetProperties($data);
            $module->refCourseId = $course->id;

            $status = T::Find('status')
                ->Where('code', '=','PND')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            if(!$status->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'not  status' ,'success'=>false]);

            if(Auth::GetActiveUser()->role->code != "ADM") {
                $module->refStatusId = $status->id;
            }

            $module->createdBy = \BerkaPhp\Helper\Auth::GetActiveUser()->id;
            $module->createdDate = DateTime::toDate(NOW, DATE_FORMAT);

            if ($module->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("le donne'e a �t� ajouter avec succ�s"), 'success'=>true, 'link'=>'/admin/modules/index/'.$courseId]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de ajouter ce donne'ee") ,'success'=>false]);
            }

        }

        $status = T::Find('status')
            ->Where('isDeleted', '=', Check::$False)
            ->FetchList(['assocArray'=>true]);
        array_unshift($status, ['id'=>"", 'name'=>DROP_DOWN, 'code'=>'...']);

        $this->view->set('status', $status);

        $this->view->set('course', $course);
        $this->view->render();
    }

    function delete($option) {

        $courseId = $option['args']['params'][1];

        $currency = T::Find('module')
            ->Where('id', '=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        if($currency != null ) {

            if(!$currency->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this currency' ,'success'=>false]);

            $currency->isDeleted = Check::$True;

            if ($currency->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("Module a �t� supprimer avec succ�s"), 'success'=>true, 'redirect'=>'/admin/modules/index/'.$courseId]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de supprimer la cat�gorie") ,'success'=>false]);
            }

        } else {
            return $this->jsonFormat(['error'=>true, 'message'=>'No data provided' ,'success'=>false]);
        }

    }


}

?>