<?php
namespace Controller\Admin;
use BerkaPhp\Controller\BerkaPhpController;
use BerkaPhp\Helper\Auth;
use BerkaPhp\Helper\DateTime;
use BerkaPhp\Helper\Debug;
use BrkORM\T;
use Helper\Check;
use Util\Helper;

class SubjectsController extends BerkaPhpController
{

    function __construct() {
        parent::__construct(false);
    }

    /* Display all users from database
    *  Client action in this controller
    *  @author berkaPhp
    */

    function index($option) {

        $moduleId = $option['args']['params'][0];

        $module = T::Find('module')
            ->Join('status', 'status.Id = module.refStatusId')
            ->Where('module.id','=', $moduleId)
            ->FetchFirstOrDefault();

        $courseId = $module->refCourseId;

        $subjects = T::Find('subject')
            ->Join('status', 'status.id = subject.refStatusId')
            ->Where('subject.isDeleted' ,'=', Check::$False)
            ->Where('subject.refModuleId' ,'=', $moduleId)
            ->OrderBy('subject.morder', 'ASC')
            ->FetchList();

        $course = T::Find('course')
            ->Join('status', 'status.Id = course.refStatusId')
            ->Where('course.id','=', $courseId)
            ->FetchFirstOrDefault();



        $this->view->set('module', $module);
        $this->view->set('course', $course);
        $this->view->set('subjects', $subjects);

        $this->view->render();

    }

    function add($option) {

        $data = $this->getPost();
        $moduleId = $option['args']['params'][0];

        $module = T::Find('module')
            ->Where('isDeleted' ,'=', Check::$False)
            ->Where('id' ,'=', $moduleId)
            ->FetchFirstOrDefault();

        $modules = T::Find('module')
            ->Where('isDeleted' ,'=', Check::$False)
            ->FetchList(['assocArray'=>true]);

        $course = T::Find('course')
            ->Where('isDeleted' ,'=', Check::$False)
            ->Where('id' ,'=', $module->refCourseId)
            ->FetchFirstOrDefault();

        if(sizeof($data) > 0) {

            if(!$module->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de ajouter ce donne'ee, pas de courr trouve'") ,'success'=>false]);

            $subject = T::Create('subject');
            $subject->SetProperties($data);
            $subject->refModuleId = $module->id;

            $status = T::Find('status')
                ->Where('code', '=','PND')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            if(!$status->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'not  status' ,'success'=>false]);

            if(Auth::GetActiveUser()->role->code != "ADM") {
                $subject->refStatusId = $status->id;
            }

            $subject->createdBy = \BerkaPhp\Helper\Auth::GetActiveUser()->id;
            $subject->createdDate = DateTime::toDate(NOW, DATE_FORMAT);

            if ($subject->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("le donne'e a �t� ajouter avec succ�s"), 'success'=>true, 'link'=>'/admin/subjects/index/'.$module->id.'/'.$course->id]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de ajouter ce donne'ee") ,'success'=>false]);
            }

        }

        $status = T::Find('status')
            ->Where('isDeleted', '=', Check::$False)
            ->FetchList(['assocArray'=>true]);

        array_unshift($status, ['id'=>"", 'name'=>DROP_DOWN, 'code'=>'...']);
        array_unshift($modules, ['id'=>"", 'name'=>DROP_DOWN]);

        $this->view->set('status', $status);
        $this->view->set('modules', $modules);
        $this->view->set('module', $module);
        $this->view->set('course', $course);
        $this->view->render();
    }
    function edit($option) {

        $data = $this->getPost();
        $id = $option['args']['params'][0];
        $moduleId = $option['args']['params'][1];

        $module = T::Find('module')
            ->Where('isDeleted' ,'=', Check::$False)
            ->Where('id' ,'=', $moduleId)
            ->FetchFirstOrDefault();

        $modules = T::Find('module')
            ->Where('isDeleted' ,'=', Check::$False)
            ->FetchList(['assocArray'=>true]);

        $course = T::Find('course')
            ->Where('isDeleted' ,'=', Check::$False)
            ->Where('id' ,'=', $module->id)
            ->FetchFirstOrDefault();

        $subject = T::Find('subject')
            ->Where('id', '=', $id)
            ->FetchFirstOrDefault();

        if(sizeof($data) > 0) {



            if(!$subject->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de modified ce donne'ee, pas de sujet trouve'") ,'success'=>false]);

            $subject->SetProperties($data);
            $subject->refModuleId = $module->id;

            $status = T::Find('status')
                ->Where('code', '=','PND')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            if(!$status->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'not  status' ,'success'=>false]);

            if(Auth::GetActiveUser()->role->code != "ADM") {
                $subject->refStatusId = $status->id;
            }

            $subject->createdBy = \BerkaPhp\Helper\Auth::GetActiveUser()->id;
            $subject->createdDate = DateTime::toDate(NOW, DATE_FORMAT);

            if ($subject->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("le donne'e a �t� ajouter avec succ�s"), 'success'=>true, 'redirect'=>'/admin/subjects/index/'.$module->id.'/'.$course->id]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de ajouter ce donne'ee") ,'success'=>false]);
            }

        }

        $status = T::Find('status')
            ->Where('isDeleted', '=', Check::$False)
            ->FetchList(['assocArray'=>true]);


        $this->view->set('subject', $subject);
        $this->view->set('status', $status);
        $this->view->set('modules', $modules);
        $this->view->set('module', $module);
        $this->view->set('course', $course);
        $this->view->render();
    }




    function delete($option) {

        $id = $option['args']['params'][0];
        $moduleId = $option['args']['params'][0];

        $currency = T::Find('subject')
            ->Where('id', '=', $id)
            ->FetchFirstOrDefault();

        if($currency != null ) {

            if(!$currency->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this currency' ,'success'=>false]);

            $currency->isDeleted = Check::$True;

            if ($currency->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("le donnee a �t� supprimer avec succ�s"), 'success'=>true, 'redirect'=>'/admin/subjects/index/'.$moduleId]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Erreur impossible de supprimer cet donnee") ,'success'=>false]);
            }

        } else {
            return $this->jsonFormat(['error'=>true, 'message'=>'No data provided' ,'success'=>false]);
        }

    }

}

?>