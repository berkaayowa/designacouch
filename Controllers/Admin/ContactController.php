<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;

    class ContactController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index($option = null) {

            $contact = T::Find('contact')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            $data = $this->getPost();

            $contact = T::Find('contact')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            if (sizeof($data) > 0) {

                if (!$contact->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => 'Opps! could not find this contact', 'success' => false]);

                $contact->SetProperties($data);

                if ($contact->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error' => false, 'message' => 'Contacts has been updated successfully', 'success' => true]);
                } else {
                    return $this->jsonFormat(['error' => true, 'message' => 'Error could not updated contact', 'success' => false]);
                }

            }

            $this->view->set('contact', $contact);
			$this->view->render();

		}



    }

?>