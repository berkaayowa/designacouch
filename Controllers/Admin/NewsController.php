<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Auth;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;


    class NewsController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $news = T::Find('news')
                ->Join('user', 'user.id = news.refUserId')
                ->Join('role', 'role.id = user.refRoleId', 'LEFT JOIN')
                ->Where('news.isDeleted', '=', Check::$False)
                ->FetchList();

            $this->view->set('news', $news);
			$this->view->render();

		}


        function edit($option = null) {

            $data = $this->getPost();

            $news = T::Find('news')
                ->Join('user', 'user.id = news.refUserId')
                ->Join('role', 'role.id = user.refRoleId', 'LEFT JOIN')
                ->Where('news.id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if(sizeof($data) > 0) {

                if(!$news->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this user' ,'success'=>false]);

                $news->SetProperties($data);
                $news->content = utf8_decode($data['content']);

                if ($news->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'News has been updated successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not updated new' ,'success'=>false]);
                }

            }

            $this->view->set('news', $news);
            $this->view->render();
        }

        function add() {

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $news = T::Create('news');
                $news->SetProperties($data);
                $news->refUserId = Auth::GetActiveUser()->id;


                if ($news->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'New has been added successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not added New' ,'success'=>false]);
                }

            }

            $this->view->render();
        }




	}

?>