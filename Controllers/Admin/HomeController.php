<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;

    class HomeController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index($option = null) {

            $home = T::Find('home')
//                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            $data = $this->getPost();


            if (sizeof($data) > 0) {

                if (!$home->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => 'Opps! could not find this contact', 'success' => false]);

                $home->SetProperties($data);

                if ($home->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error' => false, 'message' => 'Record has been updated successfully', 'success' => true]);
                } else {
                    return $this->jsonFormat(['error' => true, 'message' => 'Error could not updated a record', 'success' => false]);
                }

            }

            $this->view->set('home', $home);
			$this->view->render();

		}



    }

?>