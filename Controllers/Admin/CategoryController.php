<?php
namespace Controller\Admin;
use BerkaPhp\Controller\BerkaPhpController;
use BerkaPhp\Helper\Auth;
use BerkaPhp\Helper\DateTime;
use BerkaPhp\Helper\Debug;
use BrkORM\T;
use Helper\Check;
use Util\Helper;

class CategoryController extends BerkaPhpController
{

    function __construct() {
        parent::__construct(false);
    }

    /* Display all users from database
    *  Client action in this controller
    *  @author berkaPhp
    */

    function index() {

        $category = T::Find('category')
            ->Where('isDeleted', '=', Check::$False)
            ->FetchList();

        $this->view->set('categories', $category);

        $this->view->render();

    }

    function edit($option) {

        $data = $this->getPost();

        $category = T::Find('category')
            ->Where('id', '=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        if(sizeof($data) > 0) {

            if(!$category->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this status' ,'success'=>false]);

            $category->SetProperties($data);

            if ($category->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("A record has been updated"), 'success'=>true]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Error could not update a record") ,'success'=>false]);
            }

        }

        $this->view->set('category', $category);

        $this->view->render();
    }

    function add() {

        $data = $this->getPost();

        if(sizeof($data) > 0) {

            $category = T::Create('category');

            $category->SetProperties($data);

            if ($category->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("A record has been added"), 'success'=>true]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Error could not add a record") ,'success'=>false]);
            }

        }

        $this->view->render();
    }

    function delete($option) {

        $category = T::Find('category')
            ->Where('id', '=', $option['args']['params'][0])
            ->FetchFirstOrDefault();

        if($category != null ) {

            if(!$category->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this currency' ,'success'=>false]);

            $category->isDeleted = Check::$True;

            if ($category->Save()) {
                sleep(1);
                return $this->jsonFormat(['error'=>false, 'message'=>Helper::Utf8Text("A record has been deleted") , 'link'=>'/admin/category', 'success'=>true]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>Helper::Utf8Text("Error could not delete a record") ,'success'=>false]);
            }

        } else {
            return $this->jsonFormat(['error'=>true, 'message'=>'No data provided' ,'success'=>false]);
        }

    }


}

?>