<?php
	namespace Controller\Client;
	use BerkaPhp\Controller\BerkaPhpController;
	use BrkORM\T;

	class HomeController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Default action in this controller
        *  @author berkaPhp
        */

		function index() {
			$this->view->render();
		}

        function aboutus() {

			$aboutus = T::Find('aboutus')
				->FetchFirstOrDefault();

			$this->view->set('aboutus', $aboutus);

			$this->overWriteLayout('/Client/Layout/layoutNoBanner');
            $this->view->render();
        }

        function gallery() {
            $this->view->render();
        }

        function testimonials() {
            $this->view->render();
        }

	}

?>