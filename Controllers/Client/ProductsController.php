<?php
	namespace Controller\Client;
	use BerkaPhp\Controller\BerkaPhpController;
	use BrkORM\T;
	use Helper\Check;

	class ProductsController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct();
		}

        /* Display all users from database
        *  Default action in this controller
        *  @author berkaPhp
        */

		function index($option) {

			$categoryName = "";
			$colorName = "";

			$products = T::Find('product')
				->Join('category', 'category.id = product.refCategoryId')
				->Join('status', 'status.id = product.refStatusId')
				->Where('product.isDeleted', '=', Check::$False);

			if(sizeof($option['args']['query']) > 0 && !empty($option['args']['query']['refCategoryId'])) {

				$category = T::Find('category')
					->Where('id', '=',$option['args']['query']['refCategoryId'])
					->FetchFirstOrDefault();

				$categoryName = $category->name;

				$products->Where('product.refCategoryId', '=', $option['args']['query']['refCategoryId']);
			}

			if(sizeof($option['args']['query']) > 0 && !empty($option['args']['query']['refColorId'])) {
				$products->Where('product.refColorId', '=', $option['args']['query']['refColorId']);

				$color = T::Find('color')
					->Where('id', '=', $option['args']['query']['refColorId'])
					->FetchFirstOrDefault();

				$colorName = $color->name;
			}

			if(sizeof($option['args']['query']) > 0 && !empty($option['args']['query']['order'])) {

				$orderBy = $option['args']['query']['order'] ;

				if($orderBy == 'HigherPrice')
					$products->OrderBy('product.price', 'DESC');
				if($orderBy == 'LowerPrice')
					$products->OrderBy('product.price', 'ASC');
				if($orderBy == 'Latest')
					$products->OrderBy('product.id', 'DESC');
			}

			$products = $products->FetchList();

			$this->view->set('colorName', $colorName);
			$this->view->set('categoryName', $categoryName);
			$this->view->set('products', $products);
			$this->view->render();
		}

		function view($option) {
			$product = T::Find('product')
				->Join('category', 'category.id = product.refCategoryId')
				->Join('status', 'status.id = product.refStatusId')
				->Join('color', 'color.id = product.refColorId')
				->Where('product.isDeleted', '=', Check::$False)
				->Where('product.id', '=', $option['args']['params'][0])
				->FetchFirstOrDefault();

//			$this->overWriteLayout('/Elements/Layout/layoutNoBanner');
			$this->view->set('product', $product);
			$this->view->render();
		}

	}

?>