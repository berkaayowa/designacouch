<?php
	namespace Controller\Client;
	use BerkaPhp\Controller\BerkaPhpController;
	use BerkaPhp\Helper\DateTime;
	use BrkORM\T;

	class ContactController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct();
		}

        /* Display all users from database
        *  Default action in this controller
        *  @author berkaPhp
        */

		function index($option = null) {
			$contact = T::Find('contact')
				->FetchFirstOrDefault();

			$enq = "";

			if(isset($option['args']['query']['enq'])) {
				$enq = $option['args']['query']['enq'];
			}
			$this->view->set('contact', $contact);
			$this->view->set('enq', $enq);

			$this->overWriteLayout('/Client/Layout/layoutNoBanner');
			$this->view->render();
		}

		function send() {

			$data = $this->getPost();

			if(sizeof($data) > 0) {

				$message =@"Hi Admin, <br><br> new message has been sent from the website below are message details :<br><br> Name: " . $data['name'];
				$message.=@'<br>Email : '.$data['name'].'<br>Phone : '.$data['phoneNumber'] . '<br>Message :'.$data['message'];
				$message.=@"<br><br> Regards,<br>Design A couch";

				$sent = $this->loadComponent("Email")->send('noreply@designacouch.com', "Message from web site #". '('.DateTime::toDate(NOW).')', "", $message, 'ayowaberka@gmail.com');

				if ($sent) {
					sleep(1);
					return $this->jsonFormat(['error'=>false, 'message'=>'Your message has been sent successfully', 'success'=>true]);
				} else {
					return $this->jsonFormat(['error'=>true, 'message'=>'Your message could not be sent , try again' ,'success'=>false]);
				}

			}


		}

	}

?>