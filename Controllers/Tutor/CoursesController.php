<?php
	namespace Controller\Tutor;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;


    class CoursesController extends BerkaPhpController
	{

        private $resources ;

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index($option = null) {

            $tag = '';

            $courses = T::Find('course')
                ->Join('status', 'status.id = course.refStatusId')
                ->Join(['user'=>'tutor'], 'tutor.id = course.refUserId')
                ->Where('course.isDeleted' ,'=', Check::$False)
                ->Where('tutor.id' ,'=', $option['args']['query']['tutorId']);

            if(sizeof($option['args']['query']) > 0 && array_key_exists('tag', $option['args']['query'])) {
                $tag = $option['args']['query']['tag'];
                $courses->Where('course.name' ,' like ', '%'.$tag.'%');
            }

            $courses = $courses->FetchList();

            $this->overWriteLayout('/Elements/Layout/layoutNoSearch');
            $this->view->set('breadcrumb', ' / Me cours de vente');
            $this->view->set('courses', $courses);
			$this->view->render();

		}

        function add($option = null) {

            $categories = T::Find('category')
                ->Where('isDeleted' ,'=', Check::$False)
                ->FetchList();

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $course = T::Create('course');
                $course->SetProperties($data);

                $status = T::Find('status')
                ->Where('code', '=','PND')
                ->Where('isDeleted', '=', 0)
                ->FetchFirstOrDefault();

                if(!$status->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'not  status' ,'success'=>false]);

                $course->refStatusId = $status->id;
                $course->createdBy = \BerkaPhp\Helper\Auth::GetActiveUser()->id;

                if ($course->Save()) {

                    return $this->jsonFormat(['error'=>false, 'message'=>"le cours a ete ajoute avec succes en attente d'approbation", 'success'=>true,'link'=>'/tutor/modules?courid='.$course->id]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>"Une erreur survenue n'a pas pu sauver le cours" ,'success'=>false]);
                }

            }

            $this->overWriteLayout('/Elements/Layout/layoutNoSearch');
            $this->view->set('breadcrumb', 'Cours / Nouveau');
            $this->view->set('categories', $categories);
            $this->view->render();

        }



	}

?>