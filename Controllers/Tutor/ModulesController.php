<?php
	namespace Controller\Tutor;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;


    class ModulesController extends BerkaPhpController
	{

        private $resources ;

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index($option = null) {

            $id = $option['args']['query']['coursId'];

            $course = T::Find('course')
                ->Join('status', 'status.id = course.refStatusId')
                ->Join(['user'=>'tutor'], 'tutor.id = course.refUserId')
                ->Join('category', 'category.id = course.refCategoryId')
                ->Where('course.id' ,'=', $id)
                ->Where('course.isDeleted' ,'=', Check::$False)
                ->Where('category.isDeleted' ,'=', Check::$False)
                ->FetchFirstOrDefault();

            $this->overWriteLayout('/Elements/Layout/layoutNoSearch');
            $this->view->set('breadcrumb', 'Modules / '. $course->name);
            $this->view->set('course', $course);

			$this->view->render();

		}

        function add($option = null) {

            $this->overWriteLayout('/Elements/Layout/layoutNoSearch');
            $this->view->render();

        }



	}

?>