<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 2018-06-13
 * Time: 6:46 PM
 */

namespace Util;

use BerkaPhp\Helper\Debug;
use BerkaPhp\Helper\Rand;
use BrkORM\T;
use Helper\Check;

class Helper {

    public static function generateUniqueAccountNumber() {

        $number = Rand::uniqueDigit(100000, 999999);

        $user = T::Find('user')
            ->Where('accountNumber', '=', $number)
            ->Where('isDeleted', '=', Check::$False)
            ->FetchFirstOrDefault();

        if($user->IsAny())
            self::generateUniqueAccountNumber();
        return $number;

    }

    public static function getRate($baseId, $currencyId) {
        return  T::Find('rate')
            ->Where('refBaseCurrencyId', '=', $baseId)
            ->Where('refCurrencyId', '=', $currencyId)
            ->FetchFirstOrDefault();
    }

    public static function select($id,  $data = array(), $options, $callback = null) {

        $i_select= "<select id='{$id}' name='{$id}' ";
        $i_option = '';
        $selected = isset($options['selected']) ? $options['selected'] : '';

        $length = sizeof($data);
        $option_length = sizeof($options);

        if($option_length > 0) {

            foreach($options as $_option => $values) {
                if($_option != 'selected' && $_option != 'value' && $_option != 'text' ) {
                    $i_select.=" {$_option} ='{$values}''";
                }
            }

        }

        if($length > 0) {

            foreach($data as $data_option) {

                $sel = '';

                $data_option = (array) $data_option;

                if($selected == $data_option[$options['value']])
                {
                    $sel = 'selected';
                }
                if($callback == null)
                    $i_option.='<option '.$sel.' value ='.$data_option[$options['value']].'>'.$data_option[$options['text']].'</option>';
                else
                    $i_option.='<option '.$sel.' value ='.$data_option[$options['value']].'>'.(call_user_func($callback, $data_option)).'</option>';
            }

        } else {
            $i_option.='<option></option>';
        }

        return $i_select.'>'.$i_option.'</select>';
    }

    public static function GetCurrentUrl() {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public static function SendSMS($body = array()) {

        $post = [
            'destination' => $body['to'],
            'message' => $body['message'],
            'authkey'   => 'ab62fd7a54d842aa6799c91b161b7c22',
        ];

        $ch = curl_init('http://api.softclicktech.com/sms/request/send');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;

    }

    public static function SendPush($body = array()) {

        //https://www.youtube.com/watch?v=PPP9zyEPaCw
        $key = 'AIzaSyBzrXMtb6kCF1Stren0yThNTFSKDp2JlJk';
        $body = ['body'=>"test"];
        $post = [
            'to' => '',
            'notification' => $body
        ];

        $headers = array('Authentication: keys='.$key, 'Content-Type: application/json');

        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_HEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        curl_close($ch);

        // $key = 'AIzaSyBzrXMtb6kCF1Stren0yThNTFSKDp2JlJk';
        return $response;

    }




    public static function CalculateBalance($option) {

        $user = T::Find('user')
            ->Where('user.id', '=', $option['id'])
            ->Where('user.isDeleted', '=', Check::$False)
            ->FetchFirstOrDefault();

        $topUp = T::Find('transaction_type')
            ->Where('code', '=', TOP_UP)
            ->FetchFirstOrDefault();

        $sendMoney = T::Find('transaction_type')
            ->Where('code', '=', SEND_MONEY)
            ->FetchFirstOrDefault();

        $transferMoney = T::Find('transaction_type')
            ->Where('code', '=', TRANSFER_MONEY)
            ->FetchFirstOrDefault();

        $approved = T::Find('transaction_status')
            ->Where('code', '=', TRAN_APPROVED)
            ->FetchFirstOrDefault();

        $pending = T::Find('transaction_status')
            ->Where('code', '=', TRAN_PENDING)
            ->FetchFirstOrDefault();

        $withDraw = T::Find('transaction_status')
            ->Where('code', '=', WITHDRAW)
            ->FetchFirstOrDefault();

        $approvedTopUpTransactions = T::Find('transaction')
            ->Where('transaction.refTypeId', '=', $topUp->id)
            ->Where('transaction.refStatusId', '=', $approved->id)
            ->Where('transaction.isDeleted', '=', Check::$False)
            ->Where('transaction.createdBy', '=', $user->id);
            //->FetchList();

        $pendingTopUpTransactions = T::Find('transaction')
            ->Where('transaction.refTypeId', '=', $topUp->id)
            ->Where('transaction.refStatusId', '=', $pending->id)
            ->Where('transaction.isDeleted', '=', Check::$False)
            ->Where('transaction.createdBy', '=', $user->id);

        //->Where('transaction.refTypeId', ' IN ', '(' .$topUp->id .')')
          //  ->FetchList();


        $approvedSpendTransactions = T::Find('transaction')
            ->Where('transaction.refTypeId', '<>', $topUp->id)
            ->Where('transaction.refStatusId', '=', $approved->id)
            ->Where('transaction.isDeleted', '=', Check::$False)
            ->Where('transaction.createdBy', '=', $user->id);
            //->FetchList();

        $pendingSpendTransactions = T::Find('transaction')
            ->Where('transaction.refTypeId', '<>', $topUp->id)
            ->Where('transaction.refStatusId', '=', $pending->id)
            ->Where('transaction.isDeleted', '=', Check::$False)
            ->Where('transaction.createdBy', '=', $user->id);
            //->FetchList();

        $approvedWithDrawTransactions = T::Find('transaction')
            ->Where('transaction.refTypeId', '=', $withDraw->id)
            ->Where('transaction.refStatusId', '=', $approved->id)
            ->Where('transaction.isDeleted', '=', Check::$False)
            ->Where('transaction.createdBy', '=', $user->id);

        $pendingWithDrawTransactions = T::Find('transaction')
            ->Where('transaction.refTypeId', '=', $withDraw->id)
            ->Where('transaction.refStatusId', '=', $pending->id)
            ->Where('transaction.isDeleted', '=', Check::$False)
            ->Where('transaction.createdBy', '=', $user->id);

        $transactionsReceived = T::Find('transaction')
            ->Join(['transaction_status'=>'status'], 'status.id = transaction.refStatusId')
            ->Join(['transaction_type'=>'type'], 'type.id = transaction.refTypeId')
            ->Join('currency', 'currency.id = transaction.refCurrencyId')
            ->Join(['user'=>'author'], 'author.id = transaction.createdBy')
            ->Join(['user'=>'user'], 'user.id = transaction.refUserId')
            ->Where('user.id', '=', $user->id)
            ->Where('transaction.isDeleted', '=', Check::$False)
            ->Where('status.code', '=', TRAN_APPROVED)
            ->Where('author.id', '<>', $user->id);
//            ->Where('type.refTypeId', '<>', $withDraw->id);

        $transactionsReceivePending = T::Find('transaction')
            ->Join(['transaction_status'=>'status'], 'status.id = transaction.refStatusId')
            ->Join(['transaction_type'=>'type'], 'type.id = transaction.refTypeId')
            ->Join('currency', 'currency.id = transaction.refCurrencyId')
            ->Join(['user'=>'author'], 'author.id = transaction.createdBy')
            ->Join(['user'=>'user'], 'user.id = transaction.refUserId')
            ->Where('user.id', '=', $user->id)
            ->Where('transaction.isDeleted', '=', Check::$False)
            ->Where('status.code', '=', TRAN_PENDING)
            ->Where('author.id', '<>', $user->id);
//            ->Where('type.refTypeId', '<>', $withDraw->id);


        if(isset($option['startDate']) && isset($option['endDate'])) {
            $approvedTopUpTransactions->Where('transaction.createdDate', '>=', $option['startDate']);
            $approvedTopUpTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $pendingTopUpTransactions->Where('transaction.createdDate', '>=', $option['startDate']);
            $pendingTopUpTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $approvedSpendTransactions->Where('transaction.createdDate', '>=', $option['startDate']);
            $approvedSpendTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $pendingSpendTransactions->Where('transaction.createdDate', '>=', $option['startDate']);
            $pendingSpendTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $approvedWithDrawTransactions->Where('transaction.createdDate', '>=', $option['startDate']);
            $pendingWithDrawTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $transactionsReceived->Where('transaction.createdDate', '>=', $option['startDate']);
            $transactionsReceived->Where('transaction.createdDate', '<=', $option['endDate']);

            $transactionsReceivePending->Where('transaction.createdDate', '>=', $option['startDate']);
            $transactionsReceivePending->Where('transaction.createdDate', '<=', $option['endDate']);

        }

        else if(isset($option['endDate'])) {
            $approvedTopUpTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $pendingTopUpTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $approvedSpendTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $pendingSpendTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $pendingWithDrawTransactions->Where('transaction.createdDate', '<=', $option['endDate']);

            $transactionsReceived->Where('transaction.createdDate', '<=', $option['endDate']);

            $transactionsReceivePending->Where('transaction.createdDate', '<=', $option['endDate']);

        }

        $approvedTopUpTransactions = $approvedTopUpTransactions->FetchList();
        $pendingTopUpTransactions = $pendingTopUpTransactions->FetchList();
        $approvedSpendTransactions = $approvedSpendTransactions->FetchList();
        $pendingSpendTransactions = $pendingSpendTransactions->FetchList();
        $approvedWithDrawTransactions = $approvedWithDrawTransactions->FetchList();
        $pendingWithDrawTransactions = $pendingWithDrawTransactions->FetchList();
        $transactionsReceivedApproved = $transactionsReceived->FetchList();
        $transactionsReceivePending = $transactionsReceivePending->FetchList();

        $approvedSpendTotal = array_reduce($approvedSpendTransactions, function($i, $transaction)
        {
            return $i + $transaction->amount;
        });

        $pendingSpendTotal = array_reduce($pendingSpendTransactions, function($i, $transaction)
        {
            return $i + $transaction->amount;
        });

        $approvedTotalTopUp = array_reduce($approvedTopUpTransactions, function($i, $transaction)
        {
            return $i + $transaction->amount;
        });

        $pendingTotalTopUp = array_reduce($pendingTopUpTransactions, function($i, $transaction)
        {
            return $i + $transaction->amount;
        });

        $approvedWithDraw = array_reduce($approvedWithDrawTransactions, function($i, $transaction)
        {
            return $i + $transaction->amount;
        });

        $pendingWithDraw = array_reduce($pendingWithDrawTransactions, function($i, $transaction)
        {
            return $i + $transaction->amount;
        });

        $receiveApproved = array_reduce($transactionsReceivedApproved, function($i, $transaction)
        {
            return $i + $transaction->amount;
        });

        $receivePending = array_reduce($transactionsReceivePending, function($i, $transaction)
        {
            return $i + $transaction->amount;
        });


        $approvedSpendTotal = $approvedSpendTotal > 0 ? $approvedSpendTotal : 0;
        $pendingSpendTotal = $pendingSpendTotal > 0 ? $pendingSpendTotal : 0;
        $approvedTotalTopUp = $approvedTotalTopUp > 0 ? $approvedTotalTopUp : 0;
        $pendingTotalTopUp = $pendingTotalTopUp > 0 ? $pendingTotalTopUp : 0;

        $approvedTotalWithDraw = $approvedWithDraw > 0 ? $approvedWithDraw : 0;
        $pendingTotalWithdraw = $pendingWithDraw > 0 ? $pendingWithDraw : 0;

        $totalApprovedReceive = $receiveApproved > 0 ? $receiveApproved : 0;
        $totalPendingReceive = $receivePending > 0 ? $receivePending : 0;

        $topUp = new \stdClass();
        $spend = new \stdClass();
        $balance = new \stdClass();
        $withdraw = new \stdClass();
        $receive = new \stdClass();

        $topUp->approved = $approvedTotalTopUp;
        $topUp->pending = $pendingTotalTopUp;

        $spend->approved = $approvedSpendTotal;
        $spend->pending = $pendingSpendTotal;

        $withdraw->approved = $approvedTotalWithDraw;
        $withdraw->pending = $pendingTotalWithdraw;

        $receive->approved = $totalApprovedReceive;
        $receive->pending = $totalPendingReceive;

        $balance->approved = ($topUp->approved + $receive->approved) - ($spend->approved + $withdraw->approved);
        $balance->pending = ($topUp->pending + $receive->pending) - ($spend->pending + $withdraw->pending);

//
//        Debug::EchoVal($topUp->approved , false);
//        Debug::EchoVal($receive->approved, false);
//        Debug::EchoVal(13);
        $userBalance = new \stdClass();
        $userBalance->spend = $spend;
        $userBalance->topup = $topUp;
        $userBalance->receive = $receive;
        $userBalance->balance = $balance;
        $userBalance->user = $user;

      //  Debug::PrintOut($userBalance);
        return $userBalance;
    }

    public  static  function Compare($a, $b) {
        if($a->total_posts == $b->total_posts){
            return 0 ;
        }
        return ($a->total_posts < $b->total_posts) ? -1 : 1;
    }

    public static function Utf8Text($valur) {
        return utf8_encode($valur);
    }



} 