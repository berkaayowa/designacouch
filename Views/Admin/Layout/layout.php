<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= empty($title) ? 'Econorme' : ucfirst($title) ?></title>

    <link rel="stylesheet" type="text/css" href="/Views/Admin/Layout/css/bootstrap.min.css">
    <link rel="stylesheet" href="/Views/Admin/Layout/css/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" href="/Views/Shared/Css/FontAwesome/font-awesome.css"/>
    <link rel="stylesheet" href="/Views/Shared/Css/Other/croppie.css"/>
    <link rel="stylesheet" href="/Views/Shared/Css/Other/select2.css" />
    <link rel="stylesheet" href="/Views/Admin/Layout/css/styles.css" />
    <link rel="stylesheet" href="/Views/Admin/Layout/css/style.css" />
    <link rel="stylesheet" href="/Views/Admin/Layout/css/bootstrap-imageupload.css" />
    <link rel="stylesheet" href="/Views/Admin/Layout/css/customer.css" />
    <link rel="shortcut icon" href="/Views/Asset/icon.png" type="image/x-icon">

<!--    <link rel="stylesheet" type="text/css" href="/media/css/site-examples.css?_=19472395a2969da78c8a4c707e72123a">-->
<!--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">-->
<!--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">-->
<!--    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />-->

    <script src="/Views/Client/Layout/js/jquery-2.1.1.js"></script>
</head>
<body>
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
<!--                <div class="logo">-->
<!--                    <img src="/Views/Asset/logo.png" class="logo-img" alt=""/>-->
<!--                    -->
<!--                </div>-->
<!--                <h2>Econorme</h2>-->
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group form">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">

                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, <?= ucfirst( BerkaPhp\Helper\Auth::GetActiveUser()->firstName)?> <b class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a href="/users/logout">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <li class="current"><a href="/"><i class="glyphicon glyphicon-home"></i>Home</a></li>
                    <li><a href="<?= BerkaPhp\Helper\Html::action('/products') ?>"><i class="fa fa-product-hunt"></i>Products</a></li>
                    <li class=""><a href="<?= BerkaPhp\Helper\Html::action('/colors') ?>"><i class="fa fa-paint-brush"></i>Color</a></li>
                    <li class=""><a href="<?= BerkaPhp\Helper\Html::action('/category') ?>"><i class="glyphicon glyphicon-book"></i>Category</a></li>
                </ul>
            </div>

            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <li class=""><a href="<?= BerkaPhp\Helper\Html::action('/page/aboutus') ?>"><i class="fa fa-info"></i>About Us Page</a></li>
                    <li><a href="<?= BerkaPhp\Helper\Html::action('/contact') ?>"><i class="glyphicon glyphicon-phone-alt"></i>Contact Page</a></li>
                    <li><a href="<?= BerkaPhp\Helper\Html::action('/page/home') ?>"><i class="glyphicon glyphicon-home"></i>Header Section</a></li>
                </ul>
            </div>

            <div class="sidebar content-box hidden" style="display: block;">
                <ul class="nav">
                    <li class=""><a href="<?= BerkaPhp\Helper\Html::action('/gallery') ?>"><i class="glyphicon glyphicon-book"></i>Gallery</a></li>

                </ul>
            </div>

            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <li><a href="/users/logout"><i class="glyphicon glyphicon-log-out"></i>Logout</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="">
                    {content}
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<!---->
<!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
<!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>-->
<!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>-->
<!--<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>-->
<!--<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>-->
<!--<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>-->
<!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>-->
<!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>-->

<script src="/Views/Client/Layout/js/bootstrap.min.js"></script>
<script src="/Views/Shared/Scripts/Ckeditor/ckeditor.js"></script>
<script src="/Views/Shared/Scripts/Bootstrap/bootstrap3-wysihtml5.all.js"></script>
<script src="/Views/Shared/Scripts/Other/croppie.js"></script>
<script src="/Views/Admin/Layout/js/loader.js"></script>
<script src="/Views/Admin/Layout/js/confirmation.js"></script>
<script src="/Views/Admin/Layout/js/notification.js"></script>
<script src="/Views/Shared/Scripts/Moment/moment.js"></script>
<!--<script src="/Views/Admin/Layout/js/Jquery_table.js"></script>-->
<script src="/Views/Shared/Scripts/Bootstrap/daterangepicker.js"></script>
<script src="/Views/Shared/Scripts/Select2/select2.full.js"></script>
<script src="/BerkaPhp/Template/Utility/Javascript/app.js"></script>
<script src="/Views/Admin/Layout/js/app.js"></script>









