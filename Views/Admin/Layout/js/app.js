/**
 * Created by berka on 2017/12/31.
 */
var mts = {};

mts.TableToExcel = (function() {
    var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
    return function(table, name, fileName) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

        var link = document.createElement("A");
        link.href = uri + base64(format(template, ctx));
        link.download = fileName || 'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
})();


mts.initExcelExport = function () {


    $('[data-to-excel]').each(function (e) {

        var fileName = $(this).data('to-excel');
        var tname = $(this).attr('table-name');
        $(this).click(function(){
            mts.TableToExcel(tname, 'ReportCoin', fileName +'.xls');
        });

    })


}
mts.initLogin = function (){

    $('#login-form').validate({
        rules:{
            Email:{required:true},
            Password:{required:true}
        },
        messages: {
            Email:{required:'Email is required'},
            Password:{required:'Password is required'}
        },
        submitHandler: function(form) {

            return true;
        }
    });

    $('#login-form').validate({
        rules:{
            Email:{required:true},
            Password:{required:true}
        },
        messages: {
            Email:{required:'Email is required'},
            Password:{required:'Password is required'}
        },
        submitHandler: function(form) {

            return true;
        }
    });
};

mts.initSignup = function (){

    $('#register-form').validate({
        rules:{
            FirstName:{required:true},
            LastName:{required:true},
            Email:{required:true},
            Password:{required:true, minlength: 5},
            ConfirmPassword:{required:true, minlength: 5, equalTo: "#Password-register"}
        },
        messages: {
            FirstName:{required:'First name is required'},
            LastName:{required:'Last name is required'},
            Email:{required:'Email address is required'},
            Password:{required:'Password is required'},
            ConfirmPassword:{required:'Please confirm your password', equalTo:'Passwords does not match!'}
        },
        submitHandler: function(form) {

            $('#ajaxLoader').loader('setMessage', 'Registering...').loader('show');

            var data = {};

            $('#register-form input').each(function() {
                data[$(this).attr('name')] = $(this).val();
            });

            berkaPhpJs.request({
                url:'/users/signup',
                type: 'POST',
                data: data,
                message: 'Registering...'
            }, function(success, result) {

                if(result.success) {
                    $('#register-form input').each(function() {
                        $(this).val('');
                    });

                    $('#information').modal('show');
                }

            });

            return false;
        }
    });
};

mts.initFormRequest = function (){

    $('[data-request]').each(function() {

        $(this).on('submit', function(e) {

            e.preventDefault();

            var url = $(this).data('request');
            var type = $(this).attr('request-type');
            var message = $(this).attr('message');
            var id  = $(this).attr('id');
            var responseType = $(this).attr('response-type');
            var responseOn = $(this).attr('response-on');

            var formId = $(this).attr('id');

            var data = {};

            var form = $(this);
            var formdata = false;
            if (window.FormData){

                formdata = new FormData(form[0]);

                var length = $('#' + formId +' [data-image-cropper]').length;
                var count = 0;

                if(length == 0) {
                    berkaPhpJs.request({
                        url: url,
                        type: type,
                        data: formdata ? formdata : form.serialize(),
                        hasFile: true,
                        message: message
                    }, function(success, result) {

                        var s = result;

                        if(result['link'] != null) {
                            setTimeout(function () {
                                window.location = result['link'];
                            }, 1000);
                        }


                        if(responseType == 'html' && result['content']) {
                            $(responseOn).html(result['content'])
                            return ;
                        }

                        if(result.success) {

                        }

                    });
                } else {

                    $('#' + formId +' [data-image-cropper]').each(function() {
                        var id = $(this).attr('identity');
                        var element = $(this).data('image-cropper');
                        $(element).croppie('result', 'blob').then(function (data) {
                            formdata.append(id, data, id +'.png');

                            if(count == length - 1) {
                                berkaPhpJs.request({
                                    url: url,
                                    type: type,
                                    data: formdata ? formdata : form.serialize(),
                                    hasFile: true,
                                    message: message
                                }, function(success, result) {

                                    var s = result;

                                    if(result['link'] != null)
                                        window.location = result['link'];

                                    if(responseType == 'html' && result['content']) {
                                        $(responseOn).html(result['content'])
                                        return ;
                                    }

                                    if(result.success) {

                                    }

                                });
                            }

                            count++;
                        });
                    })
                }


            } else {
                berkaPhpJs.request({
                    url: url,
                    type: type,
                    data: formdata ? formdata : form.serialize(),
                    hasFile: true,
                    message: message
                }, function(success, result) {

                    var s = result;

                    if(result['link'] != null)
                        window.location = result['link'];

                    if(responseType == 'html' && result['content']) {
                        $(responseOn).html(result['content'])
                        return ;
                    }

                    if(result.success) {

                    }

                });
            }


        })
    });

};


mts.initSearch = function() {
    $('[data-select]').each(function() {
        var curentElement = $(this);
        $(this).searchselect({
            spinner:'Views/Client/Assets/spinner.gif',
            source: function(searchText, callback) {
                $.get('/client/ajax/type/' + curentElement.data('type')+'', function(data) {
                    data = $.parseJSON(data);
                    callback(data);
                });
            },
            onItemMap: function(item) {

                var word = curentElement.data('text');
                var words = word.split('-');
                var label = '';

                var obj = { value: item[curentElement.data('value')]};

                if(words.length > 0)
                {
                    for(var i= 0; i< words.length; i++){
                        label = label + ' ' + item[words[i]]
                    }

                } else
                {
                    label = item[word];
                }

                obj['label'] = label;

                return obj
            },
            selected: {
                id:curentElement.data('select'),
                text:curentElement.data('select-text')
            }
            ,
            searchable: true,
            onItemSelect: function(item){
                console.log(item);
            }


        })
    })
};

mts.initSearchUser = function() {
    $('[data-search-sender]').each(function() {

        var curentElement = $(this);
        $(this).searchselect({
            source: function(searchText, callback) {
                $.get('/client/ajax/type/Users', function(data) {
                    data = $.parseJSON(data);
                    callback(data);
                });
            },
            onItemMap: function(item) {
                return { value: item['UserID'], label: item['FirstName'] + ' ' + item['LastName']};
            }
            ,
            searchable: true,
            selected: {
                id:null,
                text:null
            },
            onItemSelect: function(item){
                console.log(item);
                $('#SenderName').val(item['FirstName']);
                $('#SenderSurname').val(item['LastName']);
                $('#SenderTel').val(item['Phone']);
                $('#SenderEmail').val(item['EmailAddress']);
                $('#SenderPhysicalAddress').text(item.PhysicalAddress);

                $('#sender-info').removeClass('hide');
            }

        })
    });

    $('[data-search-receiver]').each(function() {

        $(this).searchselect({
            source: function(searchText, callback) {
                $.get('/client/ajax/type/Users', function(data) {
                    data = $.parseJSON(data);
                    callback(data);
                });
            },
            onItemMap: function(item) {
                return { value: item['UserID'], label: item['FirstName'] + ' ' + item['LastName']};
            }
            ,
            searchable: true,
            onItemSelect: function(item){
                console.log(item);
                $('#ReceiverName').val(item['FirstName']);
                $('#ReceiverSurname').val(item['LastName']);
                $('#ReceiverTel').val(item['Phone']);
                $('#ReceiverEmail').val(item['EmailAddress']);

                $('#ReceiverPhysicalAddress').val(item.PhysicalAddress);
                $('#receiver-info').removeClass('hide');

            }

        })
    });
};

mts.generatePin = function(callback) {

    berkaPhpJs.request({
        url:'/client/setting/generatepin',
        message: 'Generating pin...',
        showLoader: false
    }, function(success, result) {
        if(result.success) {
            callback(result['pin']);
        }
    });

};


mts.initTransaction = function() {

    $('#ExistingSender').on('input', function () {
        if($(this).val() == 'YES') {
            $('#SenderSearchID').removeAttr('disabled');
        } else {
            $('#SenderSearchID').attr('disabled', true);
        }
    });

    $('#ExistingReceiver').on('input', function () {
        if($(this).val() == 'YES') {
            $('#ReceiverSearchID').removeAttr('disabled');
        } else {
            $('#ReceiverSearchID').attr('disabled', true);
        }
    });

    $('#btnNewPin').on('click', function(e) {
        mts.generatePin(function(pin) {
            $('#TransactionPIN').val(pin);
        });
    });

    $('#TransactionAmount').on('input', function() {
        var amount = $(this).val();

        if(amount == "")
            amount = 0;
        var interestRate = 0
        $('[data-transaction-amount]').text(amount);

        if(interestRate == "")
            interestRate = 0;

        interestRate = parseFloat(interestRate);
        amount = parseFloat(amount);
        var interest = parseFloat((interestRate / 100 ) * amount).toFixed(2);
        var amountDue = parseFloat((amount + parseFloat(interest))).toFixed(2);

        $('[data-total-amount]').text(amountDue);
        $('[data-interest-amount]').text(interest);


    });

    mts.initSearchUser();
}

mts.initReceive = function()  {

    function confirm(id, authCode, message, uniqueID) {
        berkaPhpJs.request({
            url: '/company/transactions/confirmation',
            type: 'POST',
            data: {transactionAuthorizationCode:authCode, confirmationPIN:id, TransactionUniqueID:uniqueID},
            message: message
        }, function(success, result) {

            if(result.success) {
                $('[data-invoice-body]').html(result['content']);
                $('#invoiceModal').modal('show');
            }

        });
    }

    $('#btnConfirm').on('click', function() {

        var msg =  $(this).attr('message');
        var id = $('#confirmationPIN').val();
        var authCode = $('#transactionAuthCode').val();
        var unique = $('#unique').val();

        $('#confirmationPIN').val('');
        $('#transactionAuthCode').val('');

        confirm(id, authCode, msg, unique)

    });
};

mts.initProof = function() {
    $('[data-print-proof]').on('click', function() {
        $('.print-menu').addClass('hidden');
        window.print();
        $('.print-menu').removeClass('hidden');
    });
};

mts.initAction = function() {
    $('[data-action]').on('click', function() {

        berkaPhpJs.request({
            url: $(this).data('action'),
            type: 'GET',
            message: $(this).attr('message')
        }, function(success, result) {

            if(result.success) {
                $('.action-wrapper').html(result['content']);
                $('#actionModal').modal('show');
                //$('#actionModal').removeClass('fade');
            }

        });

    });


};

mts.initActionButton = function() {
    $('[data-action-btn]').each(function(){

        var url = $(this).attr('data-action-btn');

        $(this).confirmation({
            onClick: function(id, event) {

                if (id == 'yes') {
                    berkaPhpJs.request({
                        url: url,
                        type: 'GET',
                        message: $(this).attr('loading')
                    }, function(success, result) {
                        if(success && result.link) {
                            setTimeout(function() {
                                window.location = result.link;
                            }, 1000)

                        }
                    });
                }
            }
        });

    })
};

mts.initPlugin = function() {
    $('[data-color-picker]').each(function(){
        $(this).colorPicker();
    })
    $('[data-editor]').each(function () {
        //CKEDITOR.replace($(this).attr('id'))
        $(this).wysihtml5();
    })

    $('[data-dropdrown]').each(function () {
        $(this).select2();
    })


};

mts.initAll = function() {

    berkaPhpJs.init();
    berkaPhpJs.initEqualizer('[data-eq]');
    mts.initFormRequest();
    mts.initSearch();
    mts.initPlugin();
    //mts.initTable();
    mts.initCropper();
    mts.initActionButton();
    mts.initExcelExport();


};

var countOrder = 0;

mts.initTable = function() {


    //$("#dataTable").tableExport({
    //    headings: true,
    //    footers: true,
    //    formats: [],
    //    fileName: "id",
    //    bootstrap: true,
    //    position: "bottom",
    //    ignoreRows: null,
    //    ignoreCols: null,
    //    trimWhitespace: true
    //});

    $('#dataTable').DataTable({
        "order": [],
        // dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });


};

mts.initCropper = function () {

    if($('[data-image-cropper]').length > 0) {
        var images = [];
        var field = {};

        function readFile(input, preview) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {

                    $(preview).croppie('bind', {
                        url: e.target.result
                    });
                    $('#productImageModel').modal('show');

                };
                reader.readAsDataURL(input.files[0]);
            }
        }



        $('[data-crop-btn]').on('click', function () {


        });

        if ($('[data-image-cropper]').length > 0) {

            $('[data-image-cropper]').each(function () {

                $($(this).data('image-cropper')).croppie({
                    viewport: {
                        width: 360,
                        height: 240
                        //type: 'square'
                    },
                    enableOrientation: true,
                    zoom:0,
                    showZoomer: true,
                    boundary: {
                        width: 450,
                        height: 300
                    }
                });

                $(this).change(function () {
                   // $('[data-crop-btn]').data('crop-btn', $(this).data('file-select'));
                    readFile(this, $(this).data('image-cropper'));
                });
            })

        }
    }
}

mts.InitAjaxText = function () {

    $('[data-ajax-text]').each(function (e) {

        var element = $(this);
        var key = $(this).attr('name');
        var url = $(this).data('ajax-text');

        berkaPhpJs.request({
            url: url,
            type: "GET",
            hasFile: true,
            showLoader: false,
        }, function (success, result) {
            if(success) {
                if( typeof result['data'][key] != 'undefined') {
                    element.text(result['data'][key]);
                }
            }
        });
    });

    $('[data-pending-balance]').each(function (e) {
        var element = $(this);
        var url = $(this).data('pending-balance');
        berkaPhpJs.request({
            url: url,
            type: "GET",
            hasFile: true,
        }, function (success, result) {
            if(success) {
                // element.text('R' + result['data']['balance']['pending']);
                element.text(parseFloat(result['data']['balance']['pending']).toFixed(2));
                if(result['data']['balance']['pending'] > 0) {
                    element.css('background', 'green');
                    element.css('color', 'white');
                } else {
                    element.css('background', 'red');
                    element.css('color', 'white');
                }
            }
        });
    });

};


mts.InitTransactionAction = function () {

    $('[data-transaction-action]').each(function () {
        $(this).on('click', function (e) {

            var element = $(this);

            var id = $(this).data('transaction-action');
            var action = $(this).attr('action');
            var comment = $('#comment_id_' + id).val();

            var data = {
                id: id,
                action: action,
                comment: comment
            };

            berkaPhpJs.request({
                url: '/admin/transactions/action',
                type: "POST",
                data: data
                // message: ""
            }, function (success, result) {
                if(success) {
                    window.location.reload();
                }
            });


        });

    });

};

mts.InitImageUploader = function () {
    $('[data-image-uploader]').each(function() {
        $(this).on('change', function (e) {

            function getElement(type, value) {

                if(type == 'attr') {
                   return $(this).attr(value);
                }
                else if(type == 'data') {
                    return $(this).data(value);
                }
            }
            

            function readFile(input, element) {

                var modal = element.attr('cropper-modal');

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        
                        var cropperContainer = element.attr('cropper-container');
                        
                        var basic = $(cropperContainer);

                        if (typeof basic.attr('crop') === 'undefined') {
                            basic = $(cropperContainer).croppie({
                                viewport: {
                                    width: 400,
                                    height: 250
                                },
                                boundary: { width: 500, height: 300 },
                                enableOrientation: true,
                                showZoomer: true
                            });

                            //$('[data-degre]').on('click', function (e) {
                            //    e.preventDefault();
                            //    basic.croppie('rotate', parseInt($(this).data('degre')));
                            //});

                            basic.attr('crop', true);

                            var btnUpload = element.attr('upload-btn')

                            $(btnUpload).on('click', function (e) {
                                e.preventDefault();

                                basic.croppie('result', 'blob').then(function (data) {

                                    var preview = element.attr('cropper-preview');
                                    var name = element.attr('id');
                                    var url = element.data('image-uploader');

                                    $(preview).attr('src', URL.createObjectURL(data));

                                    var formData = new FormData();
                                    formData.append(name, data, name + '.png');
                                    formData.append('image_date', 'value');

                                    berkaPhpJs.request({
                                        url: url,
                                        type: "POST",
                                        data: formData,
                                        formData: true,
                                        hasFile: true,
                                        message: "Image uploading..."
                                    }, function (success, result) {

                                        

                                    });
        
                                });
                            });

                        }

                        basic.croppie('bind', {
                            url: e.target.result,
                            points: [300, 300, 300, 300]
                        });

                    };

                    reader.readAsDataURL(input.files[0]);

                    $(modal).modal("show");
                }
            }

            readFile(this, $(this));

        });
    })
}

mts.InitHomeImageUploader = function () {
    $('[data-home-uploader]').each(function() {
        $(this).on('change', function (e) {

            function getElement(type, value) {

                if(type == 'attr') {
                    return $(this).attr(value);
                }
                else if(type == 'data') {
                    return $(this).data(value);
                }
            }


            function readFile(input, element) {

                var modal = element.attr('cropper-modal');

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {

                        var cropperContainer = element.attr('cropper-container');

                        var basic = $(cropperContainer);

                        //400x150
                        if (typeof basic.attr('crop') === 'undefined') {
                            basic = $(cropperContainer).croppie({
                                viewport: {
                                    width: 500,
                                    height: 250
                                },
                                boundary: { width: 530, height: 300 },
                                //enableOrientation: true,
                                showZoomer: true
                            });

                            //$('[data-degre]').on('click', function (e) {
                            //    e.preventDefault();
                            //    basic.croppie('rotate', parseInt($(this).data('degre')));
                            //});

                            basic.attr('crop', true);

                            var btnUpload = element.attr('upload-btn')

                            $(btnUpload).on('click', function (e) {
                                e.preventDefault();

                                basic.croppie('result', 'blob').then(function (data) {

                                    var preview = element.attr('cropper-preview');
                                    var name = element.attr('id');
                                    var url = element.data('image-uploader');

                                    $(preview).attr('src', URL.createObjectURL(data));

                                    var formData = new FormData();
                                    formData.append(name, data, name + '.png');
                                    formData.append('image_date', 'value');

                                    berkaPhpJs.request({
                                        url: url,
                                        type: "POST",
                                        data: formData,
                                        formData: true,
                                        hasFile: true,
                                        message: "Image uploading..."
                                    }, function (success, result) {



                                    });

                                });
                            });

                        }

                        basic.croppie('bind', {
                            url: e.target.result,
                            points: [300, 300, 300, 300]
                        });

                    };

                    reader.readAsDataURL(input.files[0]);

                    $(modal).modal("show");
                }
            }

            readFile(this, $(this));

        });
    })
}

mts.InitUserMenu = function() {
    $('[data-user-toggle]').on("click", function() {
        var state = $(this).data('user-toggle');

        if(state == "off") {
            $('[data-user-menu]').show();
            $(this).data('user-toggle', 'on');
        }else{
            $('[data-user-menu]').hide();
            $(this).data('user-toggle', 'off');
        }
    })
}


$(document).ready(function() {
    mts.initAll();
    mts.InitUserMenu();
    //mts.InitImageUploader();
});