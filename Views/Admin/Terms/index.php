
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="panel-title">
                <div class="title-box">
                    <label class="text-muted ">Terms & Conditions</label>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/terms/edit/'.$data->id)?>">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input required type="text" class="form-control" name="title" id="title" value="<?=$data->title?>">
                    </div>
                    <div class="form-group">
                        <label for="content">Content:</label>
                        <textarea required data-editor rows="10" class="form-control" name="content" id="content"><?=$data->content?></textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$data->id?>"/>
            <button type="submit" class="btn btn-success">Update </button>
        </form>
    </div>
</div>