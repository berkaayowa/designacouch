
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="panel-title">
                <div class="title-box">
                    <label class="text-muted ">Home Page</label>
                    <div class="btn-group pull-right">
                        <a href="<?= BerkaPhp\Helper\Html::action('/images/home') ?>" type="button" class="btn btn-default">
                            <i class="fa fa-image"></i> Banner Image
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/page/home/'.$data->id)?>">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input required type="text" class="form-control" name="title" id="title" value="<?=$data->title?>">
                    </div>
                    <div class="form-group">
                        <label for="content">SubTitle:</label>
                        <input required type="text" class="form-control" name="subTitle" id="subTitle" value="<?=$data->subTitle?>">
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$data->id?>"/>
            <button type="submit" class="btn btn-success">Update </button>
        </form>
    </div>
</div>