
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Product color</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/colors/add') ?>" type="button" class="btn btn-default">
                     <i class="fa fa-plus"></i>   Add Color
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">

        <?php foreach($colors as $color) :?>
            <div>
                <h2><?=$color->name?></h2>
                <div>
                    <a href="<?= BerkaPhp\Helper\Html::action('/colors/edit/'.$color->id) ?>">
                        <span class="fa fa-edit"></span> Edit
                    </a>
                    <a href="<?= BerkaPhp\Helper\Html::action('/colors/edit/'.$color->id) ?>">
                        <span class="fa fa-trash"></span> Delete
                    </a>
                </div>
            </div>
        <?php endforeach?>

    </div>
</div>