
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Updating <?=$color->name?></span></label>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating <?=$color->name?>..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/colors/edit/'.$color->id)?>">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input required type="text" class="form-control" name="name" id="name" value="<?=$color->name?>">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="x">Description:</label>
                        <textarea class="form-control" name="description" id="description" ><?=$color->description?></textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$color->id?>"/>
            <button type="submit" class="btn btn-success">Update </button>
        </form>
    </div>
</div>