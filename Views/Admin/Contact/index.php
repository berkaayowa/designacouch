
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="panel-title">
                <div class="title-box">
                    <label class="text-muted ">Contact</label>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/contact/index/'.$contact->id)?>">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="mobileNumber">Tel:</label>
                        <div class="input-group">
                            <input required type="text" class="form-control" name="mobileNumber" id="mobileNumber" value="<?=$contact->mobileNumber?>">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <div class="input-group">
                            <input required type="text" class="form-control" name="email" id="email" value="<?=$contact->email?>">
                            <div class="input-group-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="twitter">Address:</label>
                        <textarea required  rows="5" class="form-control" name="physicalAddress" id="physicalAddress"><?=$contact->physicalAddress?></textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$contact->id?>"/>
            <button type="submit" class="btn btn-success">Update</button>
        </form>
    </div>
</div>