
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">

                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/news/add') ?>" type="button" class="btn btn-default">
                        Nouvelle publication
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead class="thead-inverse">
                <tr>
                    <th>Titre</th>
                    <th>Date De la publication</th>
                    <th>Autheur</th>
                    <th>Role</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($news as $_news) :?>
                        <tr>
                            <td><?=$_news->title?></td>
                            <td><?=$_news->createdDate?></td>
                            <td><?=$_news->user->name?> <?=$_news->user->surname?></td>
                            <td><?=$_news->role->name?></td>

                            <td>
                                <a class="action" href="<?= BerkaPhp\Helper\Html::action('/news/edit/'.$_news->id) ?>">
                                    <span class="fa fa-edit"></span>
                                </a>
                            </td>
                        </tr>
                <?php endforeach?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
//        mts.InitUserBalance();
    })
</script>