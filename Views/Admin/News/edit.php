
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="panel-title">
                <div class="title-box">
                    <label class="text-muted ">Publication</label>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Sauvegarder..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/news/edit/'.$news->id)?>">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="title">Titre:</label>
                        <input required type="text" class="form-control" name="title" id="title" value="<?=$news->title?>">
                    </div>
                    <div class="form-group">
                        <label for="summary">Resume:</label>
                        <textarea required rows="6" class="form-control" name="summary" id="summary"><?=$news->summary?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="content">Contenu:</label>
                        <textarea required data-editor rows="10" class="form-control" name="content" id="content"><?=$news->content?></textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$news->id?>"/>
            <button type="submit" class="btn btn-success">Sauvegarder </button>
        </form>
    </div>
</div>