
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">New user</span></label>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator"  message="Adding new user..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/users/add')?>">
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input required type="text" class="form-control" name="name" id="name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="surname">Surname:</label>
                        <input required type="text" class="form-control" name="surname" id="surname">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="mobileNumber">Mobile Number:</label>
                        <input required type="text" class="form-control" name="mobileNumber" id="mobileNumber">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="password">Temporary Password</label>
                        <input type="text" class="form-control" name="password" id="password">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refRoleId">Role</label>
                        <?= Util\Helper::select('refRoleId', $roles, ['value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refStatusId">Status</label>
                        <?= Util\Helper::select('refStatusId', $status, ['value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>



            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
</div>