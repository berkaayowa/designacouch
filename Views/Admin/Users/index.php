
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <div class="btn-group pull-left">
                    <form method="get" action="<?= BerkaPhp\Helper\Html::action('/users') ?>" class="form-inline">
                    </form>
                </div>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/users/add') ?>" type="button" class="btn btn-default">
                        New User
                    </a>
                    <a href="#" class="btn btn-primary" table-name="userTable" data-to-excel="users">
                        Export To excel
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead class="thead-inverse">
                    <tr>
                        <th>Account #</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Address Email</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($users as $user) :?>
                    <?php

                    $show = true;

                    if(isset($min) && $min > 0 || isset($max) && $max > 0) {


                        $balance = Util\Helper::CalculateBalance(['id' => $user->id]);

                        if (isset($min) && $min > 0) {
                            if (!($balance->balance->approved >= $min)) {
                                $show = false;
                            }
                        }

                        if (isset($max) && $max > 0 && $balance->balance->approved > $max) {

                            if (!($balance->balance->approved <= $max)) {
                                $show = false;
                            }
                        }

                    }

                    ?>

                    <?php if($show) :?>

                        <tr>
                            <td><?=$user->accountNumber?></td>
                            <td><?=$user->name?> <?=$user->surname?></td>
                            <td><?=$user->mobileNumber?></td>
                            <td><?=$user->emailAddress?></td>
                            <td><?=$user->role->name?></td>

                            <td>
                                <a class="action" href="<?= BerkaPhp\Helper\Html::action('/users/edit/'.$user->id) ?>">
                                    <span class="fa fa-edit"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endif?>
                <?php endforeach?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
//        mts.InitUserBalance();
    })
</script>