
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left">
                    <span class="badge title">Transaction statement </span>
                    &nbsp;<span class="badge title"><?=$dateFrom?> - <?=$dateTo?></span>
                </label>

                <div class="btn-group pull-right">
                    <a href="#" class="btn btn-default" table-name="stmentTable" data-to-excel="statment_<?=str_replace(' ', '_', $user->name)?>">
                        Export To excel
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">

        <table id="stmentTable" style="width: 100%">
           <tr>
               <td>
                   <table>
                       <tr style="font-weight: bold; font-size: 14px;">
                           <td>Account #:</td>
                           <td align="left"><?=$user->accountNumber?></td>
                       </tr>
                       <tr style="background: white!important;font-weight: bold;font-size: 14px;">
                           <td>User :</td>
                           <td><?=$user->name . ' ' .$user->surname?></td>
                       </tr>
                       <tr style="font-weight: bold;font-size: 14px;">
                           <td>From :</td>
                           <td><?=$dateFrom?> / <?=$dateTo?></td>
                       </tr>
                       <tr style="background: white!important;font-weight: bold;">
                           <td>
                               &nbsp;
                           </td>
                           <td>
                               &nbsp;
                           </td>
                       </tr>
                   </table>
               </td>
           </tr>

            <tr>
                <td>
                    <div class="table-responsive">
                        <table class="table" id="export">
                            <thead class="thead-inverse">
                            <tr>
                                <th>Tran #</th>
                                <th>Date</th>
                                <th>Description</th>
<!--                                <th>Type</th>-->
                                <th>Currency</th>
                                <th>debit</th>
                                <th>credit</th>
<!--                                <th>Amount</th>-->
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($transactionRecords as $transaction) :?>
                                <tr class="capitilized-word">
                                    <td><?=$transaction['id']?></td>
                                    <td><?=BerkaPhp\Helper\DateTime::toDate($transaction['date'], DATE_FORMAT_To_DISPLAY)?></td>
                                    <td ><?=$transaction['description']?></td>
<!--                                    <td >--><?//=$transaction['type']?><!--</td>-->
                                    <td>ZAR</td>
                                    <td ><?=$transaction['debit']?></td>
                                    <td ><?=$transaction['credit']?></td>
<!--                                    <td>--><?//=$transaction['amount']?><!--</td>-->
                                    <td><?=$transaction['balance']?></td>
                                </tr>
                            <?php endforeach?>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>

    </div>
</div>

<script>
    $(document).ready(function () {

        mts.InitTransactionAction();

    })
</script>