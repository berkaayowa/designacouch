
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Categorie</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/category/add') ?>"  class="btn btn-default">
                        New Category
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead class="thead-inverse">
                <tr>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($categories as $category) :?>
                    <tr>
                        <td><?=$category->name?></td>
                        <td>
                            <a class="action" href="<?= BerkaPhp\Helper\Html::action('/category/edit/'.$category->id) ?>">
                                <span class="fa fa-edit"> </span> Edit
                            </a>
                            <a class="action" data-action-btn="<?= BerkaPhp\Helper\Html::action('/category/delete/'.$category->id) ?>"  confirmation-title="Deleting category"  confirmation-message="Are you sure , you want to delete  <?=$category->name?> '">
                                <span class="fa fa-trash"> </span> Delete
                            </a>
                        </td>
                    </tr>
                <?php endforeach?>
                </tbody>
            </table>
        </div>
    </div>
</div>

