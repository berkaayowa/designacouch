<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left">
                    <span class="badge title">
                        Updating Category
                    </span>
                </label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/category') ?>"  class="btn btn-default">
                        list of category
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/category/edit/'.$category->id)?>">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input required type="text" class="form-control" name="name" id="name" value="<?=$category->name?>">
                    </div>
                </div>

            </div>

            <input type="hidden" id="id" name="id" value="<?=$category->id?>">
            <button type="submit" class="btn btn-success pull-left">Update </button>

        </form>
    </div>
</div>