<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left">
                    <span class="badge title">
                        New Category
                    </span>
                </label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/category') ?>"  class="btn btn-default">
                        List of category
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Adding..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/category/add')?>">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input required type="text" class="form-control" name="name" id="name">
                    </div>
                </div>
<!--                <div class="col-md-12">-->
<!--                    <div class="form-group">-->
<!--                        <label for="summary">Summary:</label>-->
<!--                        <textarea class="form-control" name="summary" id="summary"></textarea>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-12">-->
<!--                    <div class="form-group">-->
<!--                        <label for="description">Description:</label>-->
<!--                        <textarea class="form-control" name="description" data-editor id="description"></textarea>-->
<!--                    </div>-->
<!--                </div>-->
            </div>

            <button type="submit" class="btn btn-success pull-left">Save </button>

        </form>
    </div>
</div>