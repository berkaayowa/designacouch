
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">New <?=$type->name?> transaction</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/users/add') ?>" type="button" class="btn btn-default">
                        Add client / user
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" data-toggle="validator"  message="Creating transaction..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/transactions/add?code=' .$type->code)?>">
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refRoleId">Requester</label>
                        <?= Util\Helper::select('createdBy', $users, ['value'=>'id', 'class'=>'form-control', 'data-dropdrown'=>true], function($data) {
                            return $data['name']." ".$data['surname'] ." | Tel:".$data['mobileNumber'] ." | Acc:#".$data['accountNumber'];
                        }) ?>
                    </div>
                </div>

                <?php if($type->code == "TSFM" || $type->code == "SDM") :?>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refRoleId">Receiver</label>
                        <?= Util\Helper::select('refUserId', $users, ['value'=>'id', 'class'=>'form-control', 'data-dropdrown'=>true], function($data) {
                            return $data['name']." ".$data['surname'] ." | Tel:".$data['mobileNumber'] ." | Acc:#".$data['accountNumber'];
                        }) ?>
                    </div>
                </div>
                <?php endif ?>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="refRoleId">Currency</label>
                                <?= Util\Helper::select('refCurrencyId', $currencies, ['selected'=>$currency->id,'value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="amount">Amount:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input required type="number" class="form-control" name="amount" id="amount">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-6 hidden">
                    <div class="form-group">
                        <label for="refStatusId">Send notification</label>
                        <br/>
                        <?= BerkaPhp\Helper\Form::checkBox("CanReceive")?>
                    </div>
                </div>


            </div>
            <button type="submit" class="btn btn-success">Create Transaction</button>
        </form>
    </div>
</div>