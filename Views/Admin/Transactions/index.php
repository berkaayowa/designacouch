
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Transactions</span></label>
                &nbsp;
                <a href="#" class="btn btn-default" table-name="transactionTable" data-to-excel="transactions">
                    Export To excel
                </a>
                <div class="btn-group pull-right">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                            New transaction <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?= BerkaPhp\Helper\Html::action('/transactions/add?code=TSFM') ?>">Transfer money</a></li>
                            <li><a href="<?= BerkaPhp\Helper\Html::action('/transactions/add?code=SDM') ?>">Send money</a></li>
                            <li><a href="<?= BerkaPhp\Helper\Html::action('/transactions/add?code=TPU') ?>">Top-up</a></li>
                            <li><a href="<?= BerkaPhp\Helper\Html::action('/transactions/add?code=CWL') ?>">Cash Withdraw</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead class="thead-inverse">
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th>Requested By</th>
                    <th>Created Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($transactions as $transaction) :?>
                    <tr style="background: <?=($transaction->status->code == 'APV' ? 'green' : ($transaction->status->code == 'CLD' ? 'red' : ''))?>!important; color: <?=($transaction->status->code == 'APV' ? 'white' : 'black')?>">
                        <td><?=$transaction->id?></td>
                        <td><?=$transaction->type->name?></td>
                        <td ><?=$transaction->status->name?></td>
                        <td><?=$transaction->currency->name?>(<?=$transaction->currency->code?>) <?=number_format($transaction->amount, 2)?></td>
                        <td><?=$transaction->author->name." ".$transaction->author->surname?></td>
                        <td><?=BerkaPhp\Helper\DateTime::toDate($transaction->createdDate, DATE_FORMAT_To_DISPLAY)?></td>
                        <td>

                            <a class="action"  data-action-btn="<?= BerkaPhp\Helper\Html::action('/transactions/delete/'.$transaction->id) ?>"  confirmation-title="Deleting"  confirmation-message="Are you sure you want to delete ?">
                                <span class="fa fa-trash"></span>
                            </a>

                            <a class="action" data-toggle="modal" data-target="#myModal-<?=$transaction->id?>">
                                <span class="fa fa-gear"></span> Options
                            </a>

                            <div id="myModal-<?=$transaction->id?>" class="modal fade" role="dialog">
                                <div class="modal-dialog" style="width: 380px;font-weight: bold;">
                                    <div class="modal-content" >
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" style="color: black;">Transaction Details #<?=$transaction->id?></h4>
                                        </div>
                                        <div class="modal-body">
                                            <table style="width: 100%; color: black;">
                                                <tr style="background:white!important;">
                                                    <td style="width: 50%;" class="text-right">Requested by</td>
                                                    <td class="text-left"><?=$transaction->author->name." ".$transaction->author->surname?> | <?=$transaction->author->accountNumber?></td>
                                                </tr>
                                                <tr style="background:white!important;">
                                                    <td style="width: 50%;" class="text-right">Date</td>
                                                    <td class="text-left"><?=$transaction->createdDate?></td>
                                                </tr>
                                                <tr style="background:white!important;">
                                                    <td class="text-right">Type </td>
                                                    <td class="text-left"><?=$transaction->type->name?></td>
                                                </tr>
                                                <tr style="background:white!important;">
                                                    <td class="text-right">Amount </td>
                                                    <td class="text-left"><?=$transaction->currency->name?>(<?=$transaction->currency->code?>) <?=$transaction->amount?></td>
                                                </tr>

                                                <?php if($transaction->type->code != 'TPU' && $transaction->type->code != 'CWL') :?>
                                                    <tr style="background:white!important;">
                                                        <td class="text-right">&nbsp;</td>
                                                    <td class="text-left">&nbsp;</td>
                                                    </tr>
                                                    <tr style="background:white!important;">
                                                        <td class="text-right">Receiver </td>
                                                        <td class="text-left"><?=$transaction->receiver->name." ".$transaction->receiver->surname?>
                                                    </tr>
                                                    <tr style="background:white!important;">
                                                        <td class="text-right">Mobile </td>
                                                        <td class="text-left"><?=$transaction->receiver->mobileNumber?>
                                                    </tr>
                                                    <tr style="background:white!important;">
                                                        <td class="text-right">Account N</td>
                                                        <td class="text-left"><?=$transaction->receiver->accountNumber?></td>
                                                    </tr>
                                                    <tr style="background:white!important;">
                                                        <td class="text-right">Amount </td>
                                                        <td class="text-left"><?=$transaction->currency->name?>(<?=$transaction->currency->code?>) <?=$transaction->amount?></td>
                                                    </tr>
                                                <?php endif?>

                                                <tr  style="background: <?=($transaction->status->code == 'APV' ? 'green' : 'yellow')?>!important; color: <?=($transaction->status->code == 'APV' ? 'white' : 'black')?>">
                                                    <td class="text-right">Status </td>
                                                    <td class="text-left"><?=$transaction->status->name?></td>
                                                </tr>

                                                <tr style="background:white!important;">
                                                    <td class="text-right">Comment</td>
                                                    <td class="text-left">
                                                        <textarea name="comment_id_<?=$transaction->id?>" id="comment_id_<?=$transaction->id?>" rows="3"><?=$transaction->comment?></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <?php if($transaction->status->code == 'PDG') :?>
                                                <div class="btn-group pull-left">
                                                    <button class="btn btn-success" data-transaction-action="<?=$transaction->id?>" action="approve">Approve</button>
                                                    <button class="btn btn-danger" data-transaction-action="<?=$transaction->id?>" action="cancel">Cancel</button>
                                                </div>
                                            <?php endif?>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach?>
                </tbody>
            </table>
        </div>
    </div>

    <table class="hidden" id="transactionTable">
        <thead>
        <tr>
            <th>#</th>
            <th>Type</th>
            <th>Status</th>
            <th>Currency</th>
            <th>Amount</th>
            <th>Requested By</th>
            <th>Created Date</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($transactions as $transaction) :?>
            <tr >
                <td ><?=$transaction->id?></td>
                <td><?=$transaction->type->name?></td>
                <td ><?=$transaction->status->name?></td>
                <td ><?=$transaction->currency->name?>(<?=$transaction->currency->code?>)</td>
                <td><?=number_format($transaction->amount, 2)?></td>
                <td><?=$transaction->author->name." ".$transaction->author->surname?></td>
                <td><?=BerkaPhp\Helper\DateTime::toDate($transaction->createdDate, DATE_FORMAT_To_DISPLAY)?></td>
            </tr>
        <?php endforeach?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function () {

        mts.InitTransactionAction();

    })
</script>