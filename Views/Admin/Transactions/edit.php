
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Editing transaction</span></label>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" data-toggle="validator"  message="Editing transaction..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/transactions/edit/'.$transaction->id)?>">
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refRoleId">Requester</label>
                        <?= Util\Helper::select('createdBy', $users, ['selected'=>$transaction->createdBy, 'value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refRoleId">Receiver</label>
                        <?= Util\Helper::select('refUserId', $users, ['selected'=>$transaction->refUserId, 'value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="amount">Amount:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                            <input required type="number" value="<?=$transaction->amount?>" class="form-control" name="amount" id="amount">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refRoleId">Currency</label>
                        <?= Util\Helper::select('refCurrencyId', $currencies, ['selected'=>$transaction->refCurrencyId, 'value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refRoleId">Transaction Type</label>
                        <?= Util\Helper::select('refTypeId', $types, ['selected'=>$transaction->refTypeId, 'value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refStatusId">Transaction Status</label>
                        <?= Util\Helper::select('refStatusId', $status, ['selected'=>$transaction->refStatusId, 'value'=>'id', 'text'=>'name', 'disabled'=>'true', 'class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refStatusId">Send notification</label>
                        <br/>
                        <?= BerkaPhp\Helper\Form::checkBox("CanReceive")?>
                    </div>
                </div>

            </div>
            <button type="submit" class="btn btn-success">Update Transaction</button>
        </form>
    </div>
</div>