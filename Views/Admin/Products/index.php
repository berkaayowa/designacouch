
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <div class="btn-group pull-left hidden">
                    <form method="get" action="<?= BerkaPhp\Helper\Html::action('/products') ?>" class="form-inline">
                        <div class="form-group">
                            <label for="">Statu: </label>
                            <?= Util\Helper::select('statusId', BrkORM\T::Find('status')->FetchList(['assocArray'=>true]), ['value'=>'id', 'class'=>'form-control', 'data-dropdrown'=>true], function($data) {
                                return $data['name'];
                            }) ?>
                        </div>
                        <div class="form-group">
                            <label for="">Category: </label>
                            <?= Util\Helper::select('refCategoryId', BrkORM\T::Find('category')->FetchList(['assocArray'=>true]), ['value'=>'id', 'class'=>'form-control', 'data-dropdrown'=>true], function($data) {
                                return $data['name'];
                            }) ?>
                        </div>
                        <button type="submit" class="btn btn-default" >Search Products</button>
                    </form>
                </div>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/products/add') ?>" type="button" class="btn btn-default">
                       <i class="fa fa-plus"></i> New Product
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">

        <?php foreach($products as $product) :?>
            <div class="card-columns">
                <div class="card ">
                    <div class="card-body">
                        <div class="card-title course-admin-title">
                            <h4><?=$product->name?></h4>
                            <span class="label label-danger" >R<?=$product->price?></span>
                        </div>
                        <p class="card-text"><?=$product->summary?></p>
                        <a class="action" href="<?= BerkaPhp\Helper\Html::action('/products/edit/'.$product->id) ?>" >
                            <i class="fa fa-edit"></i> Edit
                        </a>
                        <a class="action" href="<?= BerkaPhp\Helper\Html::action('/images/index/'.$product->id) ?>" >
                            <i class="fa fa-image"></i> View & add images
                        </a>
                        ||
                        <?=$product->category->name?>
                        || <i class="fa fa-clock-o"></i> <?=$product->createdDate?>
                        <a class="action" data-action-btn="<?= BerkaPhp\Helper\Html::action('/products/delete/'.$product->id) ?>"  confirmation-title="Deleting..."  confirmation-message="<?=\Util\Helper::Utf8Text("Are you sure you want to delete ?")?> <?=$product->name?> '">
                            <span class="fa fa-trash"> </span> Delete
                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach?>


    </div>
</div>

<script>
    $(document).ready(function () {
        mts.InitAjaxText();
    })
</script>