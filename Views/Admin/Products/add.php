
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">New Product</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/products') ?>" type="button" class="btn btn-default">
                        <i class="fa fa-list"></i> List of product
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form method="POST" data-toggle="validator"  message="Adding..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/products/add')?>" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="ProductShortName">Name</label>
                        <input type="text" required class="form-control " name="name" id="name" placeholder="Product Name">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="ProductDescription">Summary</label>
                        <textarea required rows="5" class="form-control " name="summary" id="summary" placeholder="Summary"></textarea>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="ProductDescription">Description</label>
                        <textarea required data-editor="true" rows="10" class="form-control " name="description" id="description" placeholder="Description"></textarea>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group">
                        <label  for="RefCategoryID">Category</label>
                        <?= Util\Helper::select('refCategoryId', BrkORM\T::Find('category')->FetchList(['assocArray'=>true]), ['value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="RefBrandID">Product Status</label>
                        <?= Util\Helper::select('refStatusId', BrkORM\T::Find('status')->FetchList(['assocArray'=>true]), ['value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="ProductShortName">Number of seat (Optional)</label>
                        <input type="number" required class="form-control " name="seats" id="seats" placeholder="Number of seats">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refColorId">Color</label>
                        <?= Util\Helper::select('refColorId', BrkORM\T::Find('color')->FetchList(['assocArray'=>true]), ['value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="ProductShortName">Price</label>
                        <input type="number" required class="form-control " name="price" id="price" placeholder="Price">
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label for="ProductShortName">Image</label>
                        <div class="input-group" data-image-uploader>
                            <input type="text" readonly required class="form-control" id="imageUrl" name="imageUrl" placeholder="Upload">
                            <div class="input-group-btn">
                                <span class="btn btn-default">
                                    <i class="fa fa-paperclip"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>