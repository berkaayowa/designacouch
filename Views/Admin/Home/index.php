
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="panel-title">
                <div class="title-box">
                    <label class="text-muted ">Home</label>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/home/index/'.$home->id)?>">
            <div class="row">
<!--                <div class="col-md-12">-->
<!--                    <div class="form-group">-->
<!--                        <label for="mobileNumber">Log Text:</label>-->
<!--                        <input required type="text" class="form-control" name="logoText" id="logoText" value="--><?//=$home->logoText?><!--">-->
<!--                    </div>-->
<!--                </div>-->
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="mobileNumber">Title:</label>
                        <input required type="text" class="form-control" name="title" id="title" value="<?=$home->title?>">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="twitter">Subtitle:</label>
                        <textarea required  rows="5" class="form-control" name="subtitle" id="subtitle"><?=$home->subtitle?></textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$home->id?>"/>
            <button type="submit" class="btn btn-success">Update</button>
        </form>
    </div>
</div>