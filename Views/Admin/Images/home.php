<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Home Banner</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/page/home/') ?>" type="button" class="btn btn-default">
                        <i class="fa fa-home"></i> Header Section
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <img src="<?=DOCUMENT_URL.$home->imageUrl?>" id="preview" class="img-responsive profile-img" class="user-image" alt="User Image">
                    </div>
                    <div class="col-md-12">
                        <div class="input-group">
                            <input  cropper-container="#profileImg" upload-btn="#btnUploadProfile" cropper-preview="#preview" cropper-modal="#myModal" data-home-uploader="/admin/images/home" type="file" accept="image/gif, image/jpeg, image/png" class="form-control uploadFile" id="upload" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-paperclip"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div src="#" id="profileImg" alt="Alternate Text"></div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-success" id="btnUploadProfile" data-dismiss="modal">Crop & Upload Image</button>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        mts.InitHomeImageUploader()
    });

</script>
