
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Product Images</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/images/maintain/'.$product->id) ?>"  class="btn btn-default">
                        <i class="fa fa-plus"></i> New Image
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">

        <?php foreach($images as $image) :?>
            <div class="col-sm-12 col-md-3">
                
                <img class="full-image" src="<?=DOCUMENT_URL.$image->url?>" alt="">
                <br>
                <br>
                <a class="action" data-action-btn="<?= BerkaPhp\Helper\Html::action('/images/delete/'.$image->id.'/'.$product->id) ?>"  confirmation-title="Deleting..."  confirmation-message="<?=\Util\Helper::Utf8Text("Are you sure you want to delete ?")?>">
                    <span class="fa fa-trash"> </span> Delete
                </a>

                <?php if( $image->isDefault == \Helper\Check::$True) : ?>
                    <span class="fa fa-thumbs-up"> </span> Default
                <?php endif ?>
                <?php if( $image->isDefault == \Helper\Check::$False) : ?>
                    <a class="action" data-action-btn="<?= BerkaPhp\Helper\Html::action('/images/setdefault/'.$image->id.'/'.$product->id) ?>"  confirmation-title="Settings..."  confirmation-message="<?=\Util\Helper::Utf8Text("Are you sure you want to set this image default one  ?")?>">
                        <span class="fa fa-gear"> </span> Set Default Product Iamge
                    </a>
                <?php endif ?>

            </div>
        <?php endforeach?>


    </div>
</div>

<script>
    $(document).ready(function () {
        mts.InitAjaxText();
    })
</script>