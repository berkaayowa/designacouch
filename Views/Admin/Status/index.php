
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Users</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/users/add') ?>" type="button" class="btn btn-default">
                        New User
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead class="thead-inverse">
                <tr>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($status as $statusData) :?>
                    <tr>
                        <td><?=$statusData->name?></td>
                        <td><?=$statusData->code?></td>
                        <td>
                            <a href="<?= BerkaPhp\Helper\Html::action('/status/edit/'.$statusData->id) ?>">
                                <span class="label label-default">Manage status</span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach?>
                </tbody>
            </table>
        </div>
    </div>
</div>