
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Updating <?=$status->name?></span></label>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating <?=$status->name?>..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/status/edit/'.$status->id)?>">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input required type="text" class="form-control" name="name" id="name" value="<?=$status->name?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="x">Code:</label>
                        <input disabled type="text" class="form-control" name="x" id="x" value="<?=$status->code?>">
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$status->id?>"/>
            <button type="submit" class="btn btn-success">Update </button>
        </form>
    </div>
</div>