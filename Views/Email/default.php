<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if IE 9 ]><html lang="en" class="ie9"><![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title>ZahraPay</title>

    <!-- Client specific styles - DO NOT REMOVE -->
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
        }

        .appleLinks a {
            color:#b4b4b4;
            text-decoration: none;
        }

        .backgroundTable {
            margin:0 auto;
            padding:0;
            width:100%;!important;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        .ReadMsgBody {
            width: 100%;
            background-color: #ebebeb;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        table td {
            border-collapse: collapse;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        @media screen and (max-width: 714px) {
            .force-row,
            .container,
            .tweet-col,
            .ecxtweet-col {
                width: 100% !important;
                max-width: 100% !important;
            }

            .container {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
            }
        }
        .ios-footer a {
            color: #aaaaaa !important;
            text-decoration: underline;
        }
    </style>
</head>

<body bgcolor="#eeeeee" style="margin:0; padding:0; -webkit-font-smoothing: antialiased; background-color: #eeeeee;" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" valign="top">

            <table bgcolor="#ffffff" border="0" width="650" cellpadding="0" cellspacing="0" class="container" style="width:650px; max-width:650px; background-color: #ffffff;">
                <tr>
                    <td width="100%" border="0" style="padding-top:20px;padding-right:20px;padding-left:20px;background-color:#ffffff">
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <p style="font-family:Arial, sans-serif;font-size:18px; color: #000000; text-align: center; margin-bottom: 0; padding-bottom: 0;">
                                        Transaction Notification
                                    </p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p style="font-family: Arial, sans-serif; text-align: center; margin-top: 0; margin-bottom: 0; padding-bottom: 8px; font-size: 14px; color: #9e9e9e; line-height: 20px;">

                                    </p>
                                </td>
                            </tr>

                            <tr><td valign="top" width="100%" style="line-height: 30px; font-size: 0" height="30;">&nbsp;</td></tr>

                            <tr>
                                <td style="font-family: Arial, sans-serif; font-size: 13px; color: #545454; text-align: center; line-height: 20px;">
                                    Hi Dear <?=$fullName?>, new transfer has been made to you via <strong> <?= BerkaPhp\Helper\Auth::GetActiveUser(true)->company->RegistrationName?> </strong>. Remember to bring your identification document and transaction PIN, when our are coming to collect.
                                    <br/>
                                    <br/>

                                    Regards,
                                    Softclick Tech (Pty) Ltd  Team
                                </td>
                            </tr>

                            <tr><td valign="top" width="100%" style="line-height: 60px; font-size: 0" height="60;">&nbsp;</td></tr>

                            <tr>
                                <td style="text-align: center;">
                                    <a href="" style="font-family: Arial, sans-serif; font-size: 13px; color: #ffffff !important;text-decoration:none !important; line-height: 100%; padding-bottom: 12px; padding-right: 20px; padding-left: 20px; padding-top: 14px; border-radius: 2px; background-color: #4595e7;">
                                        <font color="#fff">MORE INFORMATION</font>
                                    </a>
                                    <a href="" style="text-decoration: none;"></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td valign="top" width="100%" style="line-height: 70px; font-size: 0" height="70;">&nbsp;</td></tr>

            </table>
        </td>
    </tr>
</table>

</body>
</html>
