<!-- categories section -->
<?php if(sizeof($model['categories']) > 0) :?>
<section class="categories-section spad">
    <div class="container">
        <div class="section-title">
            <h2>Our Course Categories</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus.</p>
        </div>
        <div class="row">
            <!-- categorie -->
            <?php foreach ($model['categories'] as $category) :?>
            <div class="col-lg-4 col-md-6">
                <a href="/categories/detail/<?=$category->id?>/<?=$category->name?>">
                <div class="categorie-item">
                    <div class="ci-thumb set-bg" data-setbg="/Views/Asset/Category/<?=$category->image?>"></div>
                    <div class="ci-text">
                        <h5><?=$category->name?></h5>
                        <p><?=$category->summary?></p>
                        <span>120 Courses</span>
                    </div>
                </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif ?>
<!-- categories section end -->