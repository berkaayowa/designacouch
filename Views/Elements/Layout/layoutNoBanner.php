<!DOCTYPE html>
<html lang="en">
<head>

    <?= $metaData ?>

    <title>Design-A-Couch</title>
    <meta charset="UTF-8">
    <meta name="description" content="Design-A-Couch">
    <meta name="keywords" content="Design, Couch, Chair, Custom Design">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/Views/Client/Layout/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="/Views/Client/Layout/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="/Views/Client/Layout/css/owl.carousel.css" rel="stylesheet">
    <link href="/Views/Client/Layout/css/jquery-ui.css" rel="stylesheet" />
    <link href="/Views/Client/Layout/css/chocolat.css" rel="stylesheet"  type="text/css">
    <link href="/Views/Client/Layout/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/Views/Client/Layout/css/dac.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/Views/Client/Layout/css/main.css" rel="stylesheet" type="text/css" media="all"/>

    <link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <title><?= ucfirst($title) ?></title>

    <script src="/Views/Client/Layout/js/jquery-2.1.1.js"></script>

    <style>
        .w3_banner {
            min-height: inherit!important;
        }
    </style>
</head>
<body>

<!-- Banner-->
<div class="w3_banner">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1>
                    <a class="navbar-brand logo" href="#">
                        Design A Couch
                    </a>
                </h1>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/products">Home</a></li>
                    <li><a href="/home/aboutus" class="page-scroll">About Us</a></li>
                    <li><a href="/products" class="page-scroll" >Products</a></li>
<!--                    <li><a href="/home/gallery" class="page-scroll">Gallery</a></li>-->
                    <!-- <li><a href="/services/other" class="page-scroll">Other Service</a></li> -->
                    <li><a href="/contact" class="page-scroll">Contact</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>
</div>
<!-- /Banner-->

<div class="">
    {content}
</div>
<div class="copyright">
    <div class="container">
        <p>© 2018 Design a couch. All rights reserved | Design by <a href="https://softclicktech.co.za/">Softclick Tech (Pty) Ltd</a>
            <?php if(BerkaPhp\Helper\Auth::IsUserLogged()) : ?>
                Hi, <?= \BerkaPhp\Helper\Auth::GetActiveUser()->firstName?>
                | <a href="/admin">Admin Panel</a>
                | <a href="/users/logout">Log out</a>

            <?php else :?>
                | <a href="/users/login" class="">Login</a>
            <?php endif ?>
        </p>
    </div>
</div>

<script src="/Views/Client/Layout/js/bootstrap.min.js"></script>
<script src="/Views/Shared/Scripts/Ckeditor/ckeditor.js"></script>
<script src="/Views/Shared/Scripts/Bootstrap/bootstrap3-wysihtml5.all.js"></script>
<script src="/Views/Shared/Scripts/Other/croppie.js"></script>
<script src="/Views/Admin/Layout/js/loader.js"></script>
<script src="/Views/Admin/Layout/js/confirmation.js"></script>
<script src="/Views/Admin/Layout/js/notification.js"></script>
<script src="/Views/Shared/Scripts/Moment/moment.js"></script>
<!--<script src="/Views/Admin/Layout/js/Jquery_table.js"></script>-->
<script src="/Views/Shared/Scripts/Bootstrap/daterangepicker.js"></script>
<script src="/Views/Shared/Scripts/Select2/select2.full.js"></script>
<script src="/BerkaPhp/Template/Utility/Javascript/app.js"></script>
<script src="/Views/Client/Layout/js/zoom-image.js"></script>
<script src="/Views/Client/Layout/js/main.js"></script>
<script src="/Views/Admin/Layout/js/app.js"></script>
<script src="/Views/Client/Layout/js/jquery.chocolat.js"></script>
</body>
</html>






