<!DOCTYPE html>
<html lang="en">
<head>
    <title>Econorme</title>
    <meta charset="UTF-8">
    <meta name="description" content="WebUni Education Template">
    <meta name="keywords" content="webuni, education, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="/Views/Asset/icon.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="/Views/Shared/Css/Bootstrap/bootstrap.min_theme.css"/>
    <link rel="stylesheet" href="/Views/Shared/Css/FontAwesome/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Views/Shared/Css/owl.carousel.css"/>
    <link rel="stylesheet" href="/Views/Shared/Css/style.css"/>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <style>
        .page-info-section {
            height: 195px!important;

        }

        .footer-section{
            margin-top: 0!important;
            padding-top: 0px!important;
        }

        .footer-bottom{
            padding-top: 20px!important;
            margin-top: 0!important;
        }

        .hide {
            display: none;
        }

        .no-underline {
            text-decoration: none!important;
        }

        .single-course {
            padding-top: 40px;
        }

        .brkcrumb {
            margin-bottom: 31px;
        }
    </style>

</head>
<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>


<!-- Header section -->
<header class="header-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="site-logo">
<!--                    <img src="/Views/Asset/logo.png" alt="">-->
                </div>
                <div class="nav-switch">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <?php if(BerkaPhp\Helper\Auth::IsUserLogged()) : ?>
                    <div class="btn-group header-btn site-btn" data-user-toggle="off">
                        Salut, <?= \BerkaPhp\Helper\Auth::GetActiveUser()->name?>
                        <span class="dropdown-toggle">
                            <span class="caret"></span>
                        </span>

                        <ul class="dropdown-menu" data-user-menu>
                            <li class="dropdown-item">
                                <a href="/users/profile/<?=\BerkaPhp\Helper\Auth::GetActiveUser()->id?>/<?=\BerkaPhp\Helper\Auth::GetActiveUser()->name?>">Mon Profile</a>
                            </li>
                            <li class="dropdown-item">
                                <a href="/users/courses/<?=\BerkaPhp\Helper\Auth::GetActiveUser()->id?>?name=<?=\BerkaPhp\Helper\Auth::GetActiveUser()->name?>">Mes Cours</a>
                            </li>
                            <?php if(BerkaPhp\Helper\Auth::GetActiveUser()->role->code == 'ADM') : ?>
                                <li class="dropdown-item"><a href="/admin/users">Panneau d'administration</a></li>
                            <?php endif ?>
                            <li class="dropdown-item">
                                <a href="/users/logout">Se deconnecter</a>
                            </li>
                        </ul>
                    </div>
                <?php else :?>
                    <a href="/users/login" class="site-btn header-btn">Login</a>
                <?php endif ?>
                <nav class="main-menu">
                    <ul>
                        <li><a href="/home">Home</a></li>
                        <li><a href="/courses">Courses</a></li>
                        <li><a href="/news">News</a></li>
                        <li><a href="/contacts">Contact</a></li>
                        <li><a href="/page/objectifs">Objectifs & Mission</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

<div class="page-info-section set-bg" data-setbg="/Views/Asset/3.jpg">
    <div class="container">

    </div>
</div>

<section class="single-course spad pb-0">
    <div class="container">
        <?php if(!empty($breadcrumb) && $breadcrumb) :?>
            <div class="brkcrumb"><a href="#">Home / </a><span><?= !empty($breadcrumb) ? $breadcrumb : ""?></span></div>
        <?php endif ?>
        {content}
    </div>
</section>
<br>
<!-- footer section -->
<footer class="footer-section spad pb-0">
    <div class="footer-bottom">
        <div class="footer-warp">
            <ul class="footer-menu">
                <li><a href="#">Terms & Conditions</a></li>
                <li><a href="#">Register</a></li>
                <li><a href="#">Privacy</a></li>
            </ul>
            <div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved |  <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Softclick Tech (Pty) Ltd</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
        </div>
    </div>
</footer>
<!-- footer section end -->


<!--====== Javascripts & Jquery ======-->
<script src="/Views/Shared/Scripts/Other/jquery-3.2.1.min.js"></script>
<script src="/Views/Shared/Scripts/Bootstrap/bootstrap.js"></script>
<script src="/Views/Shared/Scripts/mixitup.min.js"></script>
<script src="/Views/Shared/Scripts/circle-progress.min.js"></script>
<script src="/Views/Shared/Scripts/owl.carousel.min.js"></script>
<script src="/Views/Shared/Scripts/main.js"></script>

<script src="/Views/Admin/Layout/js/loader.js"></script>
<script src="/Views/Admin/Layout/js/confirmation.js"></script>
<script src="/Views/Admin/Layout/js/notification.js"></script>
<script src="/Views/Admin/Layout/js/Jquery_table.js"></script>
<script src="/Views/Admin/Layout/js/datatables.js"></script>
<script src="/BerkaPhp/Template/Utility/Javascript/app.js"></script>
<script src="/Views/Admin/Layout/js/app.js"></script>
</body>
</html>