<!-- course section -->
<section class="course-section spad no-padding-bottom">
    <div class="course-warp">
        <div class="row course-items-area">
            <!-- course -->
            <?php if ( sizeof($model['courses']) > 0 ) :?>
                <?php foreach ($model['courses'] as $course) :?>
                <div class="mix col-lg-3 col-md-4 col-sm-6 finance">
                    <div class="course-item">
                        <a href="/courses/detail/<?=$course->id?>/<?=$course->name?>">
                            <div class="course-thumb set-bg" data-setbg="Views/Asset/Category/6.jpg">
                                <div class="price">Price: <?=$course->price?></div>
                            </div>
                        </a>
                        <div class="course-info">
                            <div class="course-text">
                                <a href="/courses/detail/<?=$course->id?>/<?=$course->name?>">
                                    <h5><?=$course->name?></h5>
                                </a>
                                <p><?=$course->summary?></p>
                                <div class="students">120 Students</div>
                            </div>
                            <div class="course-author">
                                <div class="ca-pic set-bg" data-setbg="/Views/Asset/no-person.jpg"></div>
                                <a href="/users/profile/<?=$course->tutor->id?>/<?=$course->tutor->name?>"><p><?=$course->tutor->name?></p></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            <?php else :?>
            <div class="mix col-lg-12 text-center">
                <br>
                No result found...
                <br>
                <br>
            </div>

            <?php endif?>
        </div>
    </div>
</section>
<!-- course section end -->