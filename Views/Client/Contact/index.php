
<div class="contact" id="contact">
    <div class="container">

        <div class="inner-w3">
            <h3>Contact Us</h3>
            <div class="text-center">
            <i class="fa fa-phone"></i> <?=$contact->mobileNumber?>
                <br>
                <i class="fa fa-envelope"></i> <?=$contact->email?>
                <br>
                <i class="fa fa-map"></i> <?=$contact->physicalAddress?>
            </div>
        </div>
        <div class="w3_agile_grids_top">
            <div class="contact-form-aits">
                <form action="#" method="post"  data-toggle="validator"  message="Sending..." request-type="POST" id="formUser" data-request="/contact/send">
                    <input type="text" class="margin-right" id="name" name="name" placeholder="Name" required="Name">
                    <input type="email" Name="Email" id="email" name="email" placeholder="Email" required="Email">
                    <input type="text" class="margin-right"  id="phoneNumber"  name="phoneNumber" placeholder="Phone Number" required="">
                    <input type="text" Name="Place" placeholder="City (Optional)" >
                    <div class="w3_txt"><textarea name="message" id="message" placeholder="Message" required=""><?=!empty($enq) ? "Ref: ". $enq . ',' : ''?></textarea></div>
                    <div class="clearfix"></div>
                    <div class="send-button agileits w3layouts">
                        <button class="btn btn-primary btn-theme ">SEND MESSAGE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26490.26346115301!2d18.62688334664714!3d-33.9081170112876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1dcc508a47958cb1%3A0x5ec0208ac380b059!2sDesign-A-Couch!5e0!3m2!1sen!2sza!4v1560693962637!5m2!1sen!2sza" style="border:0" allowfullscreen></iframe>

</div>
