<!DOCTYPE html>
<html lang="en">
<head>

    <?= $metaData ?>
    <!-- Meta -->
    <title>Design-A-Couch</title>
    <meta charset="UTF-8">
    <meta name="description" content="We design your own furniture with our custom-made couch, beds, and chairs at attainable prices.">
    <meta name="keywords" content="Design, Couch, Chair, Custom Design">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/Views/Client/Layout/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="/Views/Client/Layout/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="/Views/Client/Layout/css/owl.carousel.css" rel="stylesheet">
    <link href="/Views/Client/Layout/css/jquery-ui.css" rel="stylesheet" />
    <link href="/Views/Client/Layout/css/chocolat.css" rel="stylesheet"  type="text/css">
    <link href="/Views/Client/Layout/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/Views/Client/Layout/css/dac.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/Views/Client/Layout/css/main.css" rel="stylesheet" type="text/css" media="all"/>

<!--    <link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">-->
<!--    <link href="//fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">-->
    <script src="/Views/Shared/Scripts/Other/jquery-2.1.0.js"></script>
    <title><?= ucfirst($title) ?></title>
    <style>
        select#refCategoryId, select#refColorId{
            color: #fff;
            font-size: 15px;
            background-color: transparent;
            box-sizing: border-box;
            border: 1px solid #fff;
            font-weight: 600;
            padding: 8.4px;
            letter-spacing: 2px;
            outline: none;
        }
    </style>
</head>
<body>
<?php
    $categories = BrkORM\T::Find('category')->Where('isDeleted', '=',\Helper\Check::$False)->FetchList(['assocArray'=>true]);
    array_unshift($categories, ['id'=> '', 'name'=>'Select Category']);
    $colors = BrkORM\T::Find('color')->Where('isDeleted', '=', \Helper\Check::$False)->FetchList(['assocArray'=>true]);
    array_unshift($colors, ['id'=> '', 'name'=>'Select a color']);
    $homePage = \BrkORM\T::Find('home')->Where('isDeleted', '=', \Helper\Check::$False)->FetchFirstOrDefault();
?>
<!-- Banner-->
<div class="w3_banner" style="background: url(<?=DOCUMENT_URL.$homePage->imageUrl?>) no-repeat;">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1>
                    <a class="navbar-brand logo" href="#">
                        Design A Couch
                    </a>
                </h1>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/products">Home</a></li>
                    <li><a href="/home/aboutus" class="page-scroll">About Us</a></li>
                    <li><a href="/products" class="page-scroll" >Products</a></li>
<!--                    <li><a href="/home/gallery" class="page-scroll">Gallery</a></li>-->
                    <!-- <li><a href="/services/other" class="page-scroll">Other Service</a></li> -->
                    <li><a href="/contact" class="page-scroll">Contact</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>

    <div class="w3_bannerinfo">
        <h2 class="title"><?=$homePage->title?></h2>
        <p><?=$homePage->subTitle?></p>
        <!-- Search-->
        <div class="w3l_banser">
            <div id="search_form" class="search_top">
                <form action="/products" method="get">
                    <div class="w3l_sel">
                        <?= Util\Helper::select('refCategoryId', $categories, ['value'=>'id', 'class'=>'frm-field'], function($data) {
                            return $data['name'];
                        }) ?>
                    </div>
                    <div class="w3l_sel">
                        <?= Util\Helper::select('refColorId', $colors, ['value'=>'id', 'class'=>'frm-field'], function($data) {
                            return $data['name'];
                        }) ?>
                    </div>
                    <div class="w3l_sel">
                        <select id="order" name="order" onchange="change_country(this.value)" class="frm-field required">
                            <option value=''> Order By</option>
                            <option value="Latest">Latest</option>
                            <option value="HigherPrice">Higher Price</option>
                            <option value="LowerPrice">Lower Price</option>
                        </select>
                    </div>
                    <input type="submit" value="SEARCH">
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <!--/Search-->
    </div>
</div>
<!-- /Banner-->

<div class="">
    {content}
</div>
<div class="copyright">
    <div class="container">
        <p>© 2018 Design a couch. All rights reserved | Design by <a href="https://softclicktech.co.za/">Softclick Tech (Pty) Ltd</a>
        <?php if(BerkaPhp\Helper\Auth::IsUserLogged()) : ?>
            Hi, <?= \BerkaPhp\Helper\Auth::GetActiveUser()->firstName?>
                | <a href="/admin">Admin Panel</a>
            | <a href="/users/logout">Log out</a>

        <?php else :?>
            | <a href="/users/login" class="">Login</a>
        <?php endif ?>
        </p>
    </div>
</div>

<script src="/Views/Client/Layout/js/bootstrap.min.js"></script>
<script src="/Views/Shared/Scripts/Ckeditor/ckeditor.js"></script>
<script src="/Views/Shared/Scripts/Bootstrap/bootstrap3-wysihtml5.all.js"></script>
<script src="/Views/Shared/Scripts/Other/croppie.js"></script>
<script src="/Views/Admin/Layout/js/loader.js"></script>
<script src="/Views/Admin/Layout/js/confirmation.js"></script>
<script src="/Views/Admin/Layout/js/notification.js"></script>
<script src="/Views/Shared/Scripts/Moment/moment.js"></script>
<!--<script src="/Views/Admin/Layout/js/Jquery_table.js"></script>-->
<script src="/Views/Shared/Scripts/Bootstrap/daterangepicker.js"></script>
<script src="/Views/Shared/Scripts/Select2/select2.full.js"></script>
<script src="/BerkaPhp/Template/Utility/Javascript/app.js"></script>
<script src="/Views/Client/Layout/js/zoom-image.js"></script>
<script src="/Views/Client/Layout/js/main.js"></script>
<script src="/Views/Admin/Layout/js/app.js"></script>
<script src="/Views/Client/Layout/js/jquery.chocolat.js"></script>
<script type="text/javascript">
    $(function() {
        $('.view-seventh a').Chocolat();
    });
</script>
<!-- //gallery-pop-up-script -->
</body>
</html>






