<!-- Testimonials-->
<div class="testimonials" id="testimonials">
    <div class="container">
        <h3>Testimonials</h3>
        <div class="col-md-6 agileinfo">
            <h4>Who We are</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div class="wthree_rm1">
                <a href="#" data-toggle="modal" data-target="#myModal1">Read More </a>
                <div class="modal video-modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModal1">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Trinket</h5>
                            </div>
                            <div class="modal-body">
                                <img src="images/rm.jpg"  class="img-responsive" alt="tfg">
                                <p class="agileinfo1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 w3_test">
            <h4>Clients</h4>

            <div class="agile_wthree_inner_grids">
                <div class="test-agile_main_section">
                    <div id="owl-demo2" class="owl-carousel">
                        <div class="agile">
                            <div class="test-grid">
                                <p class="para-w3-agile">Lorem ipsum dolor sit amet, consectetur adipiscing elit,consectetur adipiscing elit, sed do eiusmod tempor incididunt Lorem ipsum dolor sit amet, consectetur adipiscing elit,consectetur adipiscing elit,
                                    .</p>
                                <div class="test-grid1">
                                    <div class="test-grid2">

                                        <img src="images/t1.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="test-grid2_text">
                                        <h4>Jade Warner</h4>
                                        <span>Client</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="agile">
                            <div class="test-grid">
                                <p class="para-w3-agile">Lorem ipsum dolor sit amet, consectetur adipiscing elit,consectetur adipiscing elit, sed do eiusmod tempor incididunt Lorem ipsum dolor sit amet, consectetur adipiscing elit,consectetur adipiscing elit,
                                    .</p>
                                <div class="test-grid1">
                                    <div class="test-grid2">

                                        <img src="images/t2.jpg" alt="" class="img-r">
                                    </div>
                                    <div class="test-grid2_text">
                                        <h4>Amy Warner</h4>
                                        <span>Client</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="agile">
                            <div class="test-grid">
                                <p class="para-w3-agile">Lorem ipsum dolor sit amet, consectetur adipiscing elit,consectetur adipiscing elit, sed do eiusmod tempor incididunt Lorem ipsum dolor sit amet, consectetur adipiscing elit,consectetur adipiscing elit,
                                    .</p>
                                <div class="test-grid1">
                                    <div class="test-grid2">

                                        <img src="images/t3.jpg" alt="" class="img-r">
                                    </div>
                                    <div class="test-grid2_text">
                                        <h4>James Warner</h4>
                                        <span>Client</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //agile_testimonials -->
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>
<!-- /Testimonials-->