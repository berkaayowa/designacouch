
<div class="contact" id="contact">
    <div class="container">

        <div class="inner-w3">
            <h3>Log In</h3>
        </div>
        <div class="w3_agile_grids_top">

            <div class="contact-form-aits">
                <form action="#" method="post"  data-toggle="validator"  message="Logging..." request-type="POST" id="formUser" data-request="/users/login">
                    <input type="email" class="margin-right" id="username" name="username" placeholder="Email" required="Email">
                    <input type="password"  id="password" name="password" placeholder="Password" required>
                    <div class="clearfix"></div>
                    <div class="send-button agileits w3layouts">
                        <button class="btn btn-primary">LOG IN</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
