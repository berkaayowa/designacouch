
<div class="row product ">
    <div class="col-sm-12 text-center c-m-20">

        <h1>Our Products</h1>
        <br>
        <h4>Products found : <?= sizeof($products)?></h4>
        <span>Category: <?=!empty($categoryName) ? $categoryName : 'any'?>, Color: <?=!empty($colorName) ? $colorName : 'any'?></span>

    </div>
<?php foreach($products as $product) :?>
    <?php

    $image = \BrkORM\T::Find('image')
        ->Where('refProductId', '=', $product->id)->Where('isDefault', '=', \Helper\Check::$True)
        ->Where('isDeleted', '=', \Helper\Check::$False)
        ->FetchFirstOrDefault();

    $url = "/Views/Assest/Images/noimage.jpg";

    if($image->IsAny() && !empty($image->url)){
        $url =DOCUMENT_URL.$image->url;
    }

    ?>

<div class="mix col-lg-3 col-md-6 col-sm-6 finance">
    <div class="course-item course-wrapper" data-eq>
        <div class="course-info" data-eq>
            <?php if($product->price > 0) : ?>
                <div class="price"><span>R<?=$product->price?></span></div>
            <?php endif ?>
            <a href="/products/view/<?=$product->id?>/<?=$product->name?>">
                <div class="course-text">
                    <img class="img-responsive img-thumbnail full-image" src="<?=$url?>" alt="">
                    <a href="/products/view/<?=$product->id?>/<?=$product->name?>">
                        <span class="product-title"><?=$product->name?></span>
                    </a>
                    <p class="hidden"><?= !empty($product->summary) ? \BerkaPhp\Helper\Text::Truncate($product->summary, 100) : ''?></p>
                    <div class="students">
                        <a  href="/contact/index?enq=Product <?=$product->name?>{<?=$product->id?>}">
                            <button class="btn btn-outline btn-default">
                                <i class="fa fa-envelope"></i> Enquire now
                            </button>
                        </a>
                        <a  href="/products/view/<?=$product->id?>/<?=$product->name?>">
                            <button class="btn btn-outline btn-info btn-theme">
                                <i class="fa fa-eye"></i> More Info
                            </button>
                        </a>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<?php endforeach?>
</div>