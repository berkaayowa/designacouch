<?php

    $image = \BrkORM\T::Find('image')
        ->Where('refProductId', '=', $product->id)->Where('isDefault', '=', \Helper\Check::$True)
        ->Where('isDeleted', '=', \Helper\Check::$False)
        ->FetchFirstOrDefault();

    $images = \BrkORM\T::Find('image')
        ->Where('refProductId', '=', $product->id)
        ->Where('isDeleted', '=', \Helper\Check::$False)
        ->FetchList();

    $url = "/Views/Assest/Images/noimage.jpg";

    if($image->IsAny() && !empty($image->url)){
        $url =DOCUMENT_URL.$image->url;
    }

?>

<div class="row product">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h3><i class="fa fa-product-hunt"></i> <?=$product->name?></h3>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-5">

        <div class="show" href="<?=$url?>">
            <img class="product-image-600" src="<?=$url?>" id="show-img">
        </div>
        <div class="small-img">
            <img src="/Views/Asset/Images/online_icon_right@2x.png" class="icon-left" alt="" id="prev-img">
            <div class="small-container">
                <div id="small-img-roll">
                    <?php foreach($images as $_image) : ?>
                    <img src="<?=DOCUMENT_URL.$_image->url?>" class="show-small-img" alt="">
                    <?php endforeach ?>
                </div>
            </div>
            <img src="/Views/Asset/Images/online_icon_right@2x.png" class="icon-right" alt="" id="next-img">
        </div>

    </div>
    <div class="col-sm-12 col-md-7">
        <h5>Product Details</h5>
        <br>
        <table class="table">
            <tr>
                <td>Name</td>
                <td><?=$product->name?></td>
            </tr>
            <?php if($product->price > 0) : ?>
            <tr>
                <td>Price</td>
                <td>R<?=$product->price?></td>
            </tr>
            <?php endif ?>
            <tr>
                <td>Product Category</td>
                <td><?=$product->category->name?></td>
            </tr>
            <?php if($product->refColorId > 0) : ?>
            <tr>
                <td>Color</td>
                <td><?=$product->color->name?>   <span style="background: <?=$product->color->name?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
            </tr>
            <?php endif ?>
            <?php if($product->seats > 0) : ?>
            <tr>
                <td>Number of seats</td>
                <td><?=$product->seats?></td>
            </tr>
            <?php endif ?>
            <tr>
                <td>Summary</td>
                <td><?=$product->summary?></td>
            </tr>
        </table>
    </div>
    <div class="col-sm-12 col-md-12">
        <br>
        <br>
        <a href="/contact/index?enq=Product <?=$product->name?>{<?=$product->id?>}">
            <button class="btn btn-info btn-theme">
                <i class="fa fa-envelope"></i> Contact us about this product
            </button>
        </a>
    </div>

    <?php if(!empty($product->description)) : ?>
    <div class="col-sm-12 col-md-12">
        <br>
        <br>
        <h5>Full Description</h5>
        <br>
        <?=$product->description?>
    </div>
    <?php endif ?>
</div>