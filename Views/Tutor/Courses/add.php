
<div class="row">
    <div class="col-lg-12 user-course">
        <div class="row">
            <div class="col-lg-12 pl-0 pr-0 text-right">
                <div class="card">
                    <div class="card-body">
                        <a href="/tutor/courses?tutorId=<?=\BerkaPhp\Helper\Auth::GetActiveUser()->id?>?name=<?=\BerkaPhp\Helper\Auth::GetActiveUser()->name?>"  class="btn btn-outline-dark">
                            <i class="fa fa-list"></i> Voir mes cours
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 user-course">
        <div class="row">
            <div class="col-lg-12 pl-0 pr-0 ">
                <div class="card">
                    <div class="card-body">
                        <form class="pt-4 form" data-toggle="validator" message="Registering..." request-type="POST" id="formLogin" data-request="/tutor/courses/add">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Nome du cours" required="required">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea rows="5" name="summary" id="summary" class="form-control input-style" placeholder="A propos du cours"></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?= Util\Helper::select('refCategoryId', $categories, ['value'=>'id', 'text'=>'name', 'class'=>'form-control input-style']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="number" name="price" id="price" class="form-control  input-style" placeholder="Prix du cours" required="required">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea rows="10" name="description" id="description" class="form-control input-style" placeholder="Description"></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <button type="submit" class="site-btn btn-block">Sauvegarder</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>