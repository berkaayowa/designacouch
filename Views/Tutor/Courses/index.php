
<div class="row">
    <div class="col-lg-12 user-course">
        <div class="row">
            <div class="col-lg-12 pl-0 pr-0 text-right">
                <div class="card">
                    <div class="card-body">
                        <a href="/tutor/courses/add"  class="btn btn-outline-dark">
                            <i class="fa fa-plus"></i> Ajouter un nouveau cours
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php foreach ($courses as $userCourse) :?>
        <?php

        $paymentClass = 'pending-for-confirmation';
        $isDeletable = true;

        if($userCourse->status->code == S_ACTIVE) {
            $paymentClass = 'paid';
        }

        ?>
        <div class="col-lg-12 user-course">
            <div class="row <?=$paymentClass?>">
                <div class="col-lg-2 pl-0">
                    <a href="/courses/detail/<?=$userCourse->id?>/<?=$userCourse->name?>">
                        <img src="/Views/Asset/20.jpg" alt="">
                    </a>
                </div>
                <div class="col-lg-10 pl-0">
                    <div class="course-info">
                        <div class="course-text">
                            <a href="/courses/detail/<?=$userCourse->id?>/<?=$userCourse->name?>">
                                <h5><?=$userCourse->name?></h5>
                            </a>
                            <p><?=$userCourse->summary?></p>
                        </div>
                        <div class="course-author">
                            <a href="/tutor/modules?coursId=<?=$userCourse->id?>"><i class="fa fa-plus"></i> Add Modules</a>
                            | <span> <i class="fa fa-ship"></i> <?=$userCourse->status->name?></span>
                            <?php if ($isDeletable) :?>
                                |  <a href="/users/delete?c=<?=$userCourse->id?>&u=<?=$userCourse->refUserId?>"><i class="fa fa-trash"></i> Remove</a>
                            <?php endif?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>


